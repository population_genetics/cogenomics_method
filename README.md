# Co-Genomics method for Host-Pathogen Interactions

![Co-Genomics Method Visual](/Images/Co-Genomics method.png)

A comprehensive toolkit for our host-parasite cogenomics method, incorporating [(1)](./HCV_pre-GWAs_analysis) Single Amino Acid Polymorphism generation from FASTA files, [(2)](./Indices_calculation) indices estimation, [(3)](./HCV_co_GWAs_analysis) Natural co-GWAs analysis with PLINK2, [(4)](./Generating_alpha_matrix_simulations) alpha matrix simulations and [(5)](./Plot_indices) visualization, and an [(6)](./Model_choice) ABC inference method for infection matrices. Additionally, we provide [(7)](./Numerical_estimation_alpha_matrix) a Python script designed for numerical estimations of the infection matrix for the top variant associations, derived from our co-evolution model, particularly when the disease encounter rate is high.

## Description

The Co-genomics method project includes codes and pipelines integral for the computation of indices (CSA, HS, HP, PI) and the execution of the ABC framework. All developments are derived from the research and findings documented in this [bioRxiv article](https://www.biorxiv.org/content/10.1101/2023.07.06.547816v1.full).

## Overview

This repository combines a diverse set of analyses and methods, with a primary focus on the inferences of co-evolutionary Genotype-by-Genotype interactions and infection matrices. Below is a concise overview of all major steps, each harbored in its respective directory and accompanied by their detailed README files:


### [1. Pathogen pre-GWAs & Fasta File Processing and Analysis](./Pathogen_pre-GWAs_analysis)
This section focuses primarily on the processing and analysis of FASTA files, especially those related to virus data and protein sequences. The processes convert an alignment into a presence and absence matrix of proteins and extract bi-allelic sites to associate with host bi-allelic sites (to incorporate either into co-GWAs and/or into the infection matrix inference with our indices). [Detailed README](./Pathogen_pre-GWAs_analysis/README.md)

### [2. Host-Pathogen Interaction Indices Calculation](./Indices_calculation)
The "Host-Pathogen Interaction Indices Calculation" section provides details about calculating various interaction indices (like CSA, HS, HP, PI) between host and pathogen from genetic data (VCF or VCF.gz). This involves data preprocessing, handling .tsv files, and utilizing scripts for computations and iterations over multiple files. Detailed instructions, prerequisites, and example usages are provided in the section's detailed [README](./Indices_calculation/README.md).

### [3. Natural co-GWAs Analysis with PLINK2](./Natural_co_GWAs_analysis)
Scripts and files to run the classic co-GWAs analysis using the output from [1. Pathogen pre-GWAs & Fasta File Processing and Analysis](./Pathogen_pre-GWAs_analysis) and incorporating additional covariates into the glm analysis with PLINK2. Tailored to operate on an Sun Grid Engine or SLURM computing cluster and executable through Bash. The output will be a GWAs output file for each column of the phenotype file. [Detailed README](./Natural_co_GWAs_analysis/README.md)

### [4. Generating Alpha Matrix Simulations for the ABC Model Choice](./Generating_alpha_matrix_simulations)
This segment involves the generation of simulations to evaluate the ABC model choice across various matrices, aiming to create datasets for each matrix of interest, attached to specific simulation rules, and utilizing various random draws and calculations. [Detailed README](./Generating_alpha_matrix_simulations/README.md)

### [5. Plotting of index distributions for various sampling schemes, $\delta$-thresholds and disease prevalences](./Plot_indices)
This folder contains all the scripts neccessary to visualize the distribution of the four indices CSA, HS, PI and HP for simulations run with various infection matrices, $\alpha$-thresholds, disease prevalences $\phi$ and sampling schemes of the number of infected and healthy hosts. Indices for the simulations are calculated for both the population and a random sample of infected and healthy hosts. [Detailed README](./Plot_indices/README.md)

### [6. ABC Model Choice and Cross-Validation Analysis](./Model_choice)
The scripts here perform model choice and cross-validation using Approximate Bayesian Computation (ABC) to differentiate between various infection matrices for a given combination of parameters, utilizing multiple indices as summary statistics. [Detailed README](./Model_choice/README.md)

### [7. Numerical Estimation of Infection Matrix](./Numerical_estimation_alpha_matrix)
Offering a Python script designed for numerical estimations of the infection matrix for each variant association, derived from our co-evolution model, ensuring accurate numerical estimates, particularly when the disease encounter rate is high. [Detailed README](./Numerical_estimation_alpha_matrix/README.md)


---

## General Usage Notes
Please consider the prerequisites specified in each individual README to successfully execute the scripts and pipelines. Reading through each detailed README file is recommended to comprehend specific usage, input data requirements, and methods concerning to each step.

---

## Download and Installation

#### Clone the repository

```
git clone https://gitlab.lrz.de/population_genetics/cogenomics_method.git
```

## Support
For assistance or inquiries, please [email Lukas Metzger (lukas.metzger@tum.de)](mailto:lukas.metzger@tum.de).

## Authors and Acknowledgment
All codes and pipelines for the HCV data analysis, index computation, and ABC framework were developed by Hanna Märkle (Center for Genomics & Systems Biology, New York University, NY, USA), Sona John, and Lukas Metzger (both Population Genetics, Department of Life Science Systems, School of Life Sciences) – [authors of the study](https://www.biorxiv.org/content/10.1101/2023.07.06.547816v1.full). 
M. Azim Ansari (Nuffield Department of Medicine, University of Oxford, UK) and Vincent Pedergnana (Laboratoire MIVEGEC, Montpellier, France) procured and pre-processed the sequence data before availability.

