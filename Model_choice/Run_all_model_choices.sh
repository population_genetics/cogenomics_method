#!/bin/bash


infile="../../Top_candidates/CSA_10M_add_freqs_sort_filter_0_2_hum_virus_2_200_top.txt"
prefix="CSA_top"

for alpha_threshold in {0.1,0.2,0.3} 
do
    for phi in {0.05,0.5,0.95}
    do
        echo "phi: $phi, alpha_threshold: ${alpha_threshold}"
        Rscript --vanilla Run_model_choice.R --phi ${phi} \
            --alpha_threshold ${alpha_threshold} \
            --results_dir ../../new_generation_alpha_simul_with_neutral_all/simulation_files_new \
            --mtype alltogether \
            --stype sample \
            --outdir ../../Model_choice_candidates \
            --candidate_file ${infile} \
            --outprefix ${prefix}
            
    done
done

