#$-cwd
#$-l vf=5G
#$-pe serial 2
#$-q bigmem
#$-N simul_alpha_matr
#$-t 1-18

ncv_samples=200
tol=0.05
ctype="alltogether"
outdir="path/CV_cross_validation"
resultsdir="path/new_generation_alpha_simul_with_neutral_all"
outprefix="Leave_1out_cv"



echo ${SGE_TASK_ID}
read -a arr < <(sed -n ${SGE_TASK_ID}p Cross_validation_schedule.txt)

stype=${arr[0]}
alpha_threshold=${arr[1]}
phi=${arr[2]}

echo "Running cross-validation for the following parameter combination"
echo ${stype}
echo ${alpha_threshold}
echo ${phi}

seed=$((SGE_TASK_ID*1000))

Rscript --vanilla Run_model_cross_validation.R \
	--ctype ${ctype} \
	--stype ${stype} \
	--tol ${tol} \
	--ncv_samples ${ncv_samples} \
	--resultsdir ${resultsdir} \
	--outdir ${outdir} \
	--outprefix ${outprefix} \
	--seed ${seed} \
	--alpha_threshold ${alpha_threshold} \
	--phi ${phi}


