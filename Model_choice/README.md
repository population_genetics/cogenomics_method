
# 6.) ABC Model Choice and Cross-Validation Analysis

This folder contains scripts that perform model choice and cross-validation using Approximate Bayesian Computation (ABC) to distinguish between six infection matrices (neutral, HR, GFG, iGFG, MA, PI) for a given combination of $\phi$ and $\delta$ using our four indices (CSA, HP, HS, PI) as summary statistics.

## Table of Contents
- [Overview](#overview)
- [Methodology](#methodology)
  - [Leave-one-out Cross-Validation](#leave-one-out-cross-validation)
  - [Model Choice for Top Association Candidates](#model-choice-for-top-association-candidates)
- [Scripts](#scripts)
- [Usage](#usage)
- [Dependencies](#dependencies)

## Overview
We first run a leave-one-out cross-validation using our simulated data set to test the suitability of
ABC model choice with our four indices as summary statistics to distinguish between the six different
matrices.

## Methodology

### Leave-one-out Cross-Validation
An ABC model choice is used to test the suitability of distinguishing between the six infection matrices given specific parameters. The model utilizes a leave-one-out cross-validation approach with a sample size of 500. The methodology involves using the function `cv4postpr` from the R-package `abc` and employs the rejection algorithm, retaining the top 5% of simulations for each cross-validation sample.

### Model Choice for Top Association Candidates
The top 200 associations, based on the highest values for each index from the human/HCV dataset, are selected. The ABC model choice was run for each association, filtering for the model with the highest overall Bayes factor and investigating its Bayes factors against any other model. ABC model choice is run using a simulated dataset for different matrices with specific parameters and our four indices as summary statistics. For categorizing results based on Bayes factor comparisons, three categories are defined as follows:

| **Category** | **Description** |
| ------------ | --------------- |
| Category 1   | The model with the highest Bayes factor is a non-neutral model/matrix, and the Bayes factor compared to the neutral matrix is larger than 2. |
| Category 2   | The model with the highest Bayes factor is a non-neutral model/matrix, but the Bayes factor compared to the neutral model is smaller or equal to 2.|
| Category 3   | The model with the highest Bayes factor is the neutral model. |

Associations in Category 1 are considered confident assignments to a non-neutral matrix.

## Scripts

- `Summarize_model_choice_candidates_ext.R`: A script to summarize model choice results. Utilizes the `tidyverse`, `viridis`, and `gridExtra` packages.
  
- `Summarize_model_choice_candidates.R`: A script to summarize model choice results using the `tidyverse` package.
  
- `Run_model_cross_validation.R`: Executes model choice ABC and requires the `abc`, `tidyverse`, and `argparser` packages.
  
- `Run_model_choice.R`: Runs model choice ABC, utilizing the `abc`, `tidyverse`, and `argparser` packages. 

- `Run_cross_validation_fsched.sh`: Shell script for submitting a job to a high-performance computing cluster to run cross-validation with specific memory and processor settings.

- `Run_all_model_choices.sh`: Bash script that loops through various parameter combinations (alpha_threshold and phi) and runs model choice analyses with them.

- `Generate_toplevel_summary_model_choice.R`: Generates a summary of model choice results for candidate models, using the `tidyverse` and `xtable` packages.

- `Generate_cross_validation_schedule.R`: This script generates a simulation schedule and requires the `tidyverse` package.

- `Cross_validation_schedule.txt`: Contains a schedule or parameter combinations (for cross-validation), including population/sample type, alpha_threshold, and phi values.


## Usage

## Cross-Validation
We tested the suitability of Approximate Bayesian Computation (ABC) model choice to distinguish different matrices by running a leave-one-out cross-validation for a sample size of 500. Cross-validation was run separately for the entire population and for the samples taken from the population.
Scripts in this section manage and execute cross-validation to evaluate the reliability of model choices.

1. **Generate Cross-Validation Schedule**: 
Generate a schedule for cross-validation using various parameter combinations with `Generate_cross_validation_schedule.R`. Ensure you have the `tidyverse` package installed in your R environment.
   - Script: `Generate_cross_validation_schedule.R`
   - Output: `Cross_validation_schedule.txt`
   - **Usage**: Execute the R script to generate the schedule, which outlines settings for various cross-validation runs.
```shell
Rscript Generate_cross_validation_schedule.R
```
   
2. **Perform Cross-Validation**:
Utilize `Run_cross_validation_fsched.sh` to submit a job to a high-performance computing cluster to run cross-validation with specified memory and processor settings. 
   - Scripts: `Run_model_cross_validation.R` and `Run_cross_validation_fsched.sh`
   - **Usage**: Use the shell script to manage the execution of cross-validation runs, utilizing the settings provided in the cross-validation schedule.
   
```shell
bash Run_cross_validation_fsched.sh
```

## Running Model Choice Analysis
Scripts `Run_model_choice.R` and `Run_all_model_choices.sh` facilitate performing model choices. Ensure the required R packages (`abc`, `tidyverse`, `argparser`) are installed.

   - **Usage**: Run `Run_all_model_choices.sh` to perform multiple model choice runs. 
   - `Run_model_choice.R` performs the actual model choice procedure and can be customized via command-line arguments.

```shell
bash Run_all_model_choices.sh
```


## Summarizing Model Choice Results
Scripts like `Summarize_model_choice_candidates.R`, `Summarize_model_choice_candidates_ext.R`, and `Generate_toplevel_summary_model_choice.R` process, summarize, and visualize model choice results.

- **Usage**: Execute the R scripts, ensuring paths to result files are correctly specified, to generate visualizations and summaries.
Utilize `Summarize_model_choice_candidates.R` and `Summarize_model_choice_candidates_ext.R` to summarize the model choice results. These scripts require the `tidyverse` package and additional packages (`viridis`, `gridExtra`).

```shell
Rscript Summarize_model_choice_candidates.R
Rscript Summarize_model_choice_candidates_ext.R
```

## Generating Top-Level Summary
Generate a top-level summary of model choice results for candidates using `Generate_toplevel_summary_model_choice.R`. Ensure the `tidyverse` and `xtable` packages are installed in your R environment.

```shell
Rscript Generate_toplevel_summary_model_choice.R
```

## Dependencies
- R
- abc v2.2.1 R-package






