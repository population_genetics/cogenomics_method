# Model choice ABC

require("abc")
require("tidyverse")
require("argparser")

# Function to extract the tag-value pairs from the command line parser
# Used later to add as a header to the output files
extract_params <- function(pargs){
  pargs_vec <- unlist(pargs)
  pargs_dat <- data.frame(tag=names(pargs_vec),value=as.character(pargs_vec))
  pargs_dat <- pargs_dat %>% filter(!tag%in%c("","help","opts"))
  pargs_dat <- pargs_dat %>%
    mutate(Tagvalue=paste0("#",paste(tag,value,sep=":")))
  return(pargs_dat)
}


# Add command line parser
args <- arg_parser("Command line argument parser")
args <- add_argument(args,arg="--phi",help="Get phi-value",default = "0.5", type=c("character"))
args <- add_argument(args,arg="--alpha_threshold",help="Get alpha_threshold",default = 0.1, type=c("numeric"))
args <- add_argument(args,arg="--resultsdir",help="Path to directory with the results",default = paste0(getwd(),"/","new_generation_alpha_simul_with_neutral_all"))
args <- add_argument(args,arg="--ncv_samples",help="Size of the cross-validation sample for each model",default = 10,type="numeric")
args <- add_argument(args,arg="--tol",help="Tolerance for cross-validation",default = 0.05,type="numeric")
args <- add_argument(args,arg="--stype",help="Type of 'sample': Entire population (pop) or sample (sample)",default = "pop",type="character")
args <- add_argument(args,arg="--ctype",help="Cross-validation type: neutral vs all named matrices (neutral_v_namedmat) or all named matrices against each other (namedmat) or all matrices (alltogether)", default="alltogether",type="character")
args <- add_argument(args,arg="--outdir",help="Output directory for results", default="CV_cross_validation",type="character")
args <- add_argument(args,arg="--outprefix",help="Prefix to append to all output files",default="Leave_1out_cv")
args <- add_argument(args,arg="--seed",help="Seed to use for cross-validation",default=100,type="character")

# Parse command line arguments
pargs <- parse_args(args)
phi <- pargs[["phi"]]
tol <- pargs[["tol"]]
ncv_samples <- pargs[["ncv_samples"]]
stype <- pargs[["stype"]]
wd <- pargs[["resultsdir"]]
alpha_threshold <- pargs[["alpha_threshold"]]
ctype <- pargs[["ctype"]]
outdir <- pargs[["outdir"]]
outprefix <- pargs[["outprefix"]]
seed_cv <- pargs[["seed"]]

if(!dir.exists(outdir)){
  dir.create(outdir)
}

outstart <- paste(outdir,outprefix,sep="/")

# Check if correct stype has been specified
if(!stype%in%c('pop',"sample")){stop("stype can be only \"sample\" or \"pop\"")}
if(!ctype%in%c("neutral_v_namedmat","namedmat","alltogether")){stop("mtype must be 'neutral_v_namedmat','namedmat' or 'altogether'")}


# Find all files for all models which match the criteria above
# Use the OS find command to find all files inside the current working directory
# which match the alpha_threshold, the given phi value and the given data type (entire population or sample from the population)
to_use <- system(paste('find',wd,"-type f -name",paste0("\"*_a_",alpha_threshold,"_phi_",phi,"_run_*_",stype,".txt\"")),intern=TRUE)

# Check which type of cross-validation needs to be run
# Discard all neutral file from the list of files to be read if all named matrices are only 
# supposed to be compared
if(ctype=="namedmat"){
  no_use <- grep("neutral",basename(to_use))
  if(length(no_use)>0){to_use <- to_use[-no_use]} 
}

# Read the all files in vector to use into a list and only keep the summary statistics (here the indices)
res <- lapply(to_use,function(x){
  print(paste("Reading file",x))
  re <- read_tsv(x,comment="#",col_types=paste0("i",paste(rep("d",times=16),collapse="")))
  re_out <- re %>% select(HS,PI,HP,CSA)
  return(re_out)
  })


# Extract the model which has been used to simulate the data
# Remove the path to working directory
# and then extract the same of the folder which is the name of the matrix used to simulate the data
model_info <- sapply(gsub(paste0(wd,"/"),"",to_use),function(x){
  strsplit(x,"_")[[1]][1]
})

# When cross-validation model vs all named matrices needs to be run
# Convert the names of all 'named' matrices to model 'named_mat;
if(ctype=="neutral_v_namedmat"){
  model_info[which(model_info!="neutral")] <- "named_mat"
}

# Name the list accordingly
names(res) <- model_info

# Fuse everything into a data.frame, as use the list names as id. Use column name model for the id
res_dat <- res %>% map_dfr(~ .x , .id = "model")

# Generate the vector of model indices as required by function cv4postpr (leave one-out cross validation)
models <- res_dat %>% pull(model)
# Generate a data.frame of the corresponding summary statistics 
# cv4postpr expects a data.frame,therefore a conversion to a data.frame is necessary
stats.sim <- res_dat %>% select(-model) %>% as.data.frame()

# Run the the leave one-out cross-validation
cv.modsel <- cv4postpr(models,stats.sim,nval=ncv_samples,tols=c(tol),method="rejection",seed=seed_cv)

modelcv_red <- data.frame(truemodel=cv.modsel$true,estimmodel=cv.modsel$estim[[paste0("tol",tol)]])

# Get the confusion matrix
cv.modsel_sum <- summary(cv.modsel)

# Extract the confusion matrix for each tolerance
cv.modsel_cmat <- lapply(paste0("tol",tol),function(x){
  cv.modsel_sum[["conf.matrix"]][[x]]
})
names(cv.modsel_cmat) <- paste0("tol_",tol)

# Write the output
lapply(names(cv.modsel_cmat),function(x){
  # The next three lines are needed to convert the table object into a properly structured 
  # data frame
  cmat_dat <- data.frame(cv.modsel_cmat[[x]])
  names(cmat_dat) <- c("model1","model2","Number")
  cmat_dat_wider <- cmat_dat %>% pivot_wider(names_from=model2,values_from=Number)
  # Generate the outputfile name
  outfile <- paste0(paste(outstart,x,"phi",phi,"alpha",alpha_threshold,"type",ctype,"stype",stype,"cmat",sep="_"),".tsv")
  # Write the parameter settings as comment to output file
  write(extract_params(pargs) %>% pull(Tagvalue),file=outfile)
  # Write the confusion matrix to the outputfile
  write_tsv(cmat_dat_wider,outfile,append=TRUE,col_names=TRUE)
})




# Extract the mean model misclassification probabilities. for each tolerance
cv.modsel_probs <- lapply(paste0("tol",tol),function(x){
  cv.modsel_sum[["probs"]][[x]]
})
names(cv.modsel_probs) <- paste0("tol_",tol)

# Write the output
lapply(names(cv.modsel_probs),function(x){
  # The next three lines are needed to convert the table object into a properly structured 
  # data frame
  probs_dat <- data.frame(cv.modsel_probs[[x]])
  probs_dat$model1 <- rownames(probs_dat)
  rownames(probs_dat) <- NULL
  probs_dat <- probs_dat %>% relocate(model1)
  # Generate the outputfile name
  outfile <- paste0(paste(outstart,x,"phi",phi,"alpha",alpha_threshold,"type",ctype,"stype",stype,"probs",sep="_"),".tsv")
  # Write the parameter settings as comment to output file
  write(extract_params(pargs) %>% pull(Tagvalue),file=outfile)
  # Write the confusion matrix to the outputfile
  write_tsv(probs_dat,outfile,append=TRUE,col_names=TRUE)
})
