import sys
import os
import gzip
import pandas as pd
import concurrent.futures

def calculate_allele_frequencies_v2(vcf_filepath, pathogen_data, pathogen_col, output_filepath, vcf_sample_ids):
    output_data = []
    sample_id_col = pathogen_data.columns[0]

    parasite1 = set(pathogen_data[pathogen_data[pathogen_col] == 1][sample_id_col].values)
    parasite2 = set(pathogen_data[pathogen_data[pathogen_col] == 0][sample_id_col].values)

    open_func = gzip.open if vcf_filepath.endswith(".vcf.gz") else open

    with open_func(vcf_filepath, 'rt') as file:
        for line in file:
            if line.startswith("##") or line.startswith("#"):
                continue
            
            split_line = line.strip().split("\t")
            if len(split_line) != len(vcf_sample_ids) + 9:
                raise ValueError("Incorrect number of genotype entries in VCF file line.")

            chrom, pos, var_id, ref, alt = split_line[:5]
            genotypes = split_line[9:]
            
            parasite1_allele_counts = {"ref": 0, "alt": 0}
            parasite2_allele_counts = {"ref": 0, "alt": 0}
            observed_alleles = set()

            for idx, genotype in enumerate(genotypes):
                sample_id = vcf_sample_ids[idx]
                if sample_id not in parasite1 and sample_id not in parasite2:
                    continue
                
                if genotype in ["0/0", "0|0"]:
                    allele_counts = {"ref": 2, "alt": 0}
                elif genotype in ["0/1", "1/0", "0|1", "1|0"]:
                    allele_counts = {"ref": 1, "alt": 1}
                elif genotype in ["1/1", "1|1"]:
                    allele_counts = {"ref": 0, "alt": 2}
                else:
                    continue
                
                if genotype in ["0/0", "0/1", "1/1", "0|0", "0|1", "1|0", "1|1"]:
                    observed_alleles.update([ref, alt])
                
                if sample_id in parasite1:
                    parasite1_allele_counts["ref"] += allele_counts["ref"]
                    parasite1_allele_counts["alt"] += allele_counts["alt"]
                elif sample_id in parasite2:
                    parasite2_allele_counts["ref"] += allele_counts["ref"]
                    parasite2_allele_counts["alt"] += allele_counts["alt"]
            
            parasite1_ref_af = round(parasite1_allele_counts["ref"] / (parasite1_allele_counts["ref"] + parasite1_allele_counts["alt"]), 5) if (parasite1_allele_counts["ref"] + parasite1_allele_counts["alt"]) > 0 else None
            parasite1_alt_af = round(parasite1_allele_counts["alt"] / (parasite1_allele_counts["ref"] + parasite1_allele_counts["alt"]), 5) if (parasite1_allele_counts["ref"] + parasite1_allele_counts["alt"]) > 0 else None
            parasite2_ref_af = round(parasite2_allele_counts["ref"] / (parasite2_allele_counts["ref"] + parasite2_allele_counts["alt"]), 5) if (parasite2_allele_counts["ref"] + parasite2_allele_counts["alt"]) > 0 else None
            parasite2_alt_af = round(parasite2_allele_counts["alt"] / (parasite2_allele_counts["ref"] + parasite2_allele_counts["alt"]), 5) if (parasite2_allele_counts["ref"] + parasite2_allele_counts["alt"]) > 0 else None
            
            output_data.append([chrom, pos, var_id, ref, alt, 
                                parasite1_ref_af, parasite1_alt_af, 
                                parasite2_ref_af, parasite2_alt_af,
                                parasite1_allele_counts["ref"], parasite1_allele_counts["alt"],
                                parasite2_allele_counts["ref"], parasite2_allele_counts["alt"],
                                sum(parasite1_allele_counts.values()) + sum(parasite2_allele_counts.values())])
    
    
    output_df = pd.DataFrame(
        output_data,
        columns=["CHROM", "POS", "ID", "REF_Host1", "ALT_Host2", 
                 "PARASITE1_HOST1_AF", "PARASITE1_HOST2_AF", 
                 "PARASITE2_HOST1_AF", "PARASITE2_HOST2_AF",
                 "PARASITE1_HOST1_COUNT", "PARASITE1_HOST2_COUNT",
                 "PARASITE2_HOST1_COUNT", "PARASITE2_HOST2_COUNT",
                 "TOTAL_ALLELES_OBSERVED"]
    )
    output_df.to_csv(output_filepath, index=False, sep="\t")
    return output_df
    

def merge_and_adjust_freqs(main_df, freq_df, pathogen):
    print(f"Starting with pathogen {pathogen}")

    common_cols = ['CHROM', 'POS']
    if 'ID' in main_df.columns and 'ID' in freq_df.columns:
        common_cols.append('ID')
        print("Merging on CHROM, POS, and ID...")
    else:
        print("Merging on CHROM and POS...")

    merged_df = pd.merge(main_df, freq_df, on=common_cols, how='left')

    swapped_freq_count = 0
    na_freq_count = 0
    successful_add_count = 0
    
    
    # Add a new column to store whether the alleles were swapped
    merged_df['swapped'] = "no"

    for idx, row in merged_df.iterrows():
        if pd.isna(row['UNINF_HOST1_FREQ']) or pd.isna(row['UNINF_HOST2_FREQ']):
            na_freq_count += 1
            continue
        
        # Check allele consistency
        if (
            (row['REF_Host1'], row['ALT_Host2']) == (row['REF'], row['ALT']) or
            (row['REF_Host1'], row['ALT_Host2']) == (row['ALT'], row['REF'])
           ):
            if (row['REF_Host1'] == row['ALT']) and (row['ALT_Host2'] == row['REF']):
                swapped_freq_count += 1
                merged_df.at[idx, 'UNINF_HOST1_FREQ'], merged_df.at[idx, 'UNINF_HOST2_FREQ'] = row['UNINF_HOST2_FREQ'], row['UNINF_HOST1_FREQ']
                # Update the 'swapped' column to 'yes' when the alleles were swapped
                merged_df.at[idx, 'swapped'] = "yes"
            successful_add_count += 1
        else:
            merged_df.at[idx, 'UNINF_HOST1_FREQ'], merged_df.at[idx, 'UNINF_HOST2_FREQ'] = None, None  # set to NaN for removal later
    
    print(f"Total swapped frequencies: {swapped_freq_count}")
    print(f"Total NA frequencies: {na_freq_count}")
    print(f"Total successful frequency adds: {successful_add_count}")

    merged_df = merged_df.rename(columns={'OBS_CT': 'OBS_CT_UNINF', 'TOTAL_ALLELES_OBSERVED': 'OBS_CT_INF'})
    
    # Drop the undesired columns
    merged_df = merged_df.drop(columns=['REF', 'ALT', 'FREQS'])
    
    initial_row_count = merged_df.shape[0]
    merged_df = merged_df.dropna(subset=['UNINF_HOST1_FREQ', 'UNINF_HOST2_FREQ'])
    final_row_count = merged_df.shape[0]

    print(f"Removed {initial_row_count - final_row_count} rows due to various issues (NA values, inconsistent alleles, etc.)")
    return merged_df


def process_pathogen(vcf_filepath, pathogen_data, pathogen_col, afreq_df, vcf_sample_ids, output_filepath):
    log_messages = []
    try:
        # Calculate allele frequencies
        allele_freq_df = calculate_allele_frequencies_v2(vcf_filepath, pathogen_data, pathogen_col, output_filepath, vcf_sample_ids)
        log_messages.append(f"Processed allele frequencies for pathogen {pathogen_col}")

        # Merge and adjust frequencies
        merged_df = merge_and_adjust_freqs(allele_freq_df, afreq_df, pathogen_col)
        log_messages.append(f"Merged and adjusted frequencies for pathogen {pathogen_col}")

        # Save the final output
        merged_df.to_csv(output_filepath, sep='\t', index=False)
        log_messages.append(f"Output for pathogen {pathogen_col} saved to {output_filepath}")

    except Exception as e:
        log_messages.append(f"An error occurred for pathogen {pathogen_col}: {e}")

    return log_messages



    
def main(vcf_filepath, pathogen_filepath, afreq_filepath, output_dir, specified_pathogens=None, n_cpus=1):
    pathogen_data = pd.read_csv(pathogen_filepath, sep="\t")
    sample_id_col = pathogen_data.columns[0]

    # Perform header format check and sample ID consistency check
    open_func = gzip.open if vcf_filepath.endswith(".vcf.gz") else open
    with open_func(vcf_filepath, 'rt') as file:
        for line in file:
            if line.startswith("##"):
                continue
            elif line.startswith("#"):
                vcf_header_columns = line.strip().split("\t")
                expected_headers = ["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT"]
                if vcf_header_columns[:9] != expected_headers:
                    raise ValueError(f"Incorrect header format in VCF file. Expected: {expected_headers}, Found: {vcf_header_columns[:9]}")
                vcf_sample_ids = vcf_header_columns[9:]
                                # Check for consistency between sample IDs
                if list(pathogen_data[sample_id_col]) != vcf_sample_ids:
                    raise ValueError("Sample IDs or their order are inconsistent between the pathogen and genotype files.")
                break

    # Handle specified pathogens or use all pathogens if none specified
    if specified_pathogens and specified_pathogens != "":
        binary_pathogens = specified_pathogens.split(",")
        invalid_cols = [col for col in binary_pathogens if col not in pathogen_data.columns]
        if invalid_cols:
            raise ValueError(f"The following specified pathogen columns do not exist in the pathogen file: {', '.join(invalid_cols)}")
    else:
        binary_pathogen_columns = pathogen_data.drop(columns=[sample_id_col]).apply(
            lambda col: set(col.unique()).issubset({0, 1})
        )
        binary_pathogens = binary_pathogen_columns.index[binary_pathogen_columns].tolist()

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    
    # Read .afreq file
    freq_df = pd.read_csv(afreq_filepath, sep='\t', comment='#', header=None, dtype=str)
    freq_df.columns = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'FREQS', 'OBS_CT']
    freq_df[['UNINF_HOST1_FREQ', 'UNINF_HOST2_FREQ']] = freq_df['FREQS'].str.split(',', n=1, expand=True)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Parallel processing
    log_messages_all = []
    with concurrent.futures.ProcessPoolExecutor(max_workers=n_cpus) as executor:
        futures = {executor.submit(process_pathogen, vcf_filepath, pathogen_data, pathogen_col, freq_df, vcf_sample_ids, os.path.join(output_dir, f"output_{pathogen_col}.tsv")): pathogen_col for pathogen_col in binary_pathogens}
        
        for future in concurrent.futures.as_completed(futures):
            log_messages = future.result()
            log_messages_all.extend(log_messages)

    # Print log messages in an orderly fashion
    for message in log_messages_all:
        print(message)

if __name__ == "__main__":
    vcf_file = sys.argv[1]
    pathogen_file = sys.argv[2]
    afreq_file = sys.argv[3]
    output_directory = sys.argv[4]
    specified_pathogens = sys.argv[5] if len(sys.argv) > 5 else None
    n_cpus = int(sys.argv[6]) if len(sys.argv) > 6 else 1

    try:
        main(vcf_file, pathogen_file, afreq_file, output_directory, specified_pathogens, n_cpus)
    except ValueError as e:
        print(f"Error: {e}", file=sys.stderr)
    except Exception as e:
        print(f"An unexpected error occurred: {e}", file=sys.stderr)
