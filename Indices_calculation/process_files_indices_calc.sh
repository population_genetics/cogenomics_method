#!/bin/bash

# Example usage: bash run_process_files.sh /path_to_your_files/

DIRECTORY=$1

for file in "$DIRECTORY"*.tsv
do
    python calculate_indices.py "$file"
done

