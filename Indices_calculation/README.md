# 2.) Host-Pathogen Interaction Indices Calculation

## Table of Contents
- [Overview](#overview)
- [Indices Definition and Equations](#indices-definition-and-equations)
- [Prerequisites](#prerequisites)
- [Repository Contents](#repository-contents)
- [Workflow Overview](#workflow-overview)
- [Tutorial](#detailed-steps)
  - [Input Files](#Input-Files)
  - [Extract Frequencies from healthy host Dataset](#extract-frequencies-from-healthy-host-dataset)
  - [Merge and Adjust Frequencies](#merge-and-adjust-frequencies)
  - [Calculate Indices](#calculate-indices)
- [Notes](#notes)

## Overview
This directory includes scripts for calculating four indices: CSA, HS, HP, and PI. These indices are instrumental in analyzing host-pathogen association frequencies within a GxG interaction matrix. The calculations use allele frequencies from both infected and non-infected diploid hosts.


## Indices Definition and Equations

- **CSA (Cross-Species Association) Index**: 

Assesses the association between the genotype of infected hosts and the genotype of the respective infecting pathogen strains.

![CSA Equation](https://latex.codecogs.com/svg.latex?%5Ctext%7BCSA%7D%20%3D%20%5Cleft%20%5Clvert%20%5Cfrac%7B%5Ctilde%7Bf%7D_%7B11%7D%20%5Ctilde%7Bf%7D_%7B22%7D%20-%20%5Ctilde%7Bf%7D_%7B12%7D%5Ctilde%7Bf%7D_%7B21%7D%7D%7B%5Cbar%7Bf%7D_1%7D%20%5Cright%20%5Crvert)
  
- **HS (Host Susceptibility) Index**: 

Compares allele frequencies in the infected versus non-infected host subsamples.

![HS Equation](https://latex.codecogs.com/svg.latex?%5Ctext%7BHS%7D%20%3D%20%5Cleft%20%5Clvert%5Cfrac%7B%28f_%7B11%7D&plus;f_%7B12%7D%29f_%7B2z%7D-%28f_%7B21%7D&plus;f_%7B22%7D%29f_%7B1z%7D%7D%7B%5Cbar%7Bf%7D_2%7D%20%5Cright%20%5Crvert)
  
- **PI (Pathogen Infectivity) Index**: 

Assesses the difference between pathogen allele frequencies.

![PI Equation](https://latex.codecogs.com/svg.latex?%5Ctext%7BPI%7D%20%3D%20%5Cleft%20%5Clvert%20%5Cfrac%7Bf_%7B12%7Df_%7B22%7D-f_%7B11%7Df_%7B21%7D%7D%7B%5Cbar%7Bf%7D_2%7D%20%5Cright%20%5Crvert)
  
- **HP (Host Partitioning) Index**: 

Reflects the difference of allele frequencies of one host genotype infected by one pathogen allele and when non-infected.

![HP Equation](https://latex.codecogs.com/svg.latex?%5Ctext%7BHP%7D%20%3D%20%5Cleft%20%5Clvert%20%5Cfrac%7Bf_%7B12%7Df_%7B2z%7D-f_%7B21%7Df_%7B1z%7D%7D%7B%5Cbar%7Bf%7D_2%7D%20%5Cright%20%5Crvert)

Where:  
![f_1 Equation](https://latex.codecogs.com/svg.latex?%5Cbar%7Bf%7D_1%20%3D%20%5Csqrt%7B%28%5Ctilde%7Bf%7D_%7B11%7D&plus;%5Ctilde%7Bf%7D_%7B12%7D%29%28%5Ctilde%7Bf%7D_%7B21%7D&plus;%5Ctilde%7Bf%7D_%7B22%7D%29%28%5Ctilde%7Bf%7D_%7B11%7D&plus;%5Ctilde%7Bf%7D_%7B21%7D%29%28%5Ctilde%7Bf%7D_%7B12%7D&plus;%5Ctilde%7Bf%7D_%7B22%7D%29%7D)  

![f_2 Equation](https://latex.codecogs.com/svg.latex?%5Cbar%7Bf%7D_2%20%3D%20%28f_%7B11%7D&plus;f_%7B12%7D&plus;f_%7B1z%7D%29%28f_%7B21%7D&plus;f_%7B22%7D&plus;f_%7B2z%7D%29)

## Prerequisites

- Infected host genotype file (in VCF or VCF.GZ format) containing all infected host samples.
- Pathogen file: Must match the host file in terms of sample IDs (in the same order), with the first column containing sample IDs and subsequent columns indicating binary SAAPs.
- **Programming Language:** Python
- **Python Libraries:** 
  - `pandas`
  - `numpy`
  - `sys`
  - `gzip`
  - `concurrent.futures`
- **Shell:** Bash (Unix shell)
- **PLINK2**

## Repository Contents

- `EstimateandCombineFrequencies.py`: script processes VCF (Variant Call Format) files to calculate allele frequencies, merges these calculations with additional frequency data from healthy hosts, and outputs one file per pathogen SAAP. It is designed to work in a parallel processing environment, handling multiple pathogens simultaneously.
- `run_EstimateandCombineFrequencies.sh`: Bash script for batch processing using `EstimateandCombineFrequencies.py`.
- `healthy_50_host_randomized_freq.afreq`: Output file from PLINK2 for healthy frequencies.
- `calculate_indices.py`: Calculates CSA, HS, HP, and PI indices.
- `process_files_indices_calc.sh`: This script iterates over `.tsv` files and applies `calculate_indices.py` to each.
- `healthy_52_host_samples_randomized.vcf`: Example VCF file for the healthy host sample.
- `infected_50_human_samples_randomized.vcf`: Example VCF file for the infected host sample.
- `README.md`: Documentation file.
- `Tutorial_FinalOutput`: Final output files from allele frequency and indices calculation.

## Workflow Overview

1. Extract allele frequencies from a healthy host dataset using PLINK2.
2. The script processes VCF (Variant Call Format) files to calculate allele frequencies, merges these calculations with additional frequency data from healthy hosts, and outputs the results. It is designed to work in a parallel processing environment, handling multiple SAAPs simultaneously. [`EstimateandCombineFrequencies.py`](./EstimateandCombineFrequencies.py).
3. Calculate the indices (CSA, HS, HP, PI) using [`calculate_indices.py`](./calculate_indices.py).

## Tutorial

### Input files

Before proceeding with our analysis, it's crucial to review all the required input files. The necessary files include:

A) Pathogen File.

B) Infected Host Alleles VCF File: This file should contain the variant call format (VCF) data for host alleles affected by the pathogen. It's important to ensure that the sample names in this file correspond exactly with those in the pathogen file.

C) Healthy Host Alleles VCF File: This file must include the VCF data for host alleles that are not affected by the pathogen. It should feature the same variants as those present in the Infected Host Alleles VCF File, allowing for a direct comparison between healthy and infected alleles.

Each of these files has a specific structure, which is outlined below:

##### A) Example Pathogen file input from [(1.) Pathogen_pre-GWAs_analysis](../Pathogen_pre-GWAs_analysis/) named [output_example_50_pathogen_samples_SAAP.txt](../Pathogen_pre-GWAs_analysis/output_example_50_pathogen_samples_SAAP.txt). Here are the first 5 rows:

|ID      |V1_M|V2_X|V2_S|V2_T|V3_T|V3_R|V3_Y|V5_P|V6_K|V7_P|V12_X|V13_R|V13_P|V13_Y|V16_V|V17_R|V18_R|V18_Y|V18_Q|V20_Q|V22_V|V32_G|V32_S|V32_X|V34_V|V35_Y|V36_V|V37_L|V39_R|V40_R|V47_R|V50_R|
|--------|----|----|----|----|----|----|----|----|----|----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|sample1 |1   |1   |0   |0   |1   |0   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |1    |1    |1    |1    |1    |
|sample2 |0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |1    |0    |1    |0    |0    |1    |1    |1    |1    |1    |1    |1    |0    |
|sample3 |1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |1    |0    |0    |0    |1    |0    |1    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample4 |0   |0   |0   |1   |0   |1   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample5 |1   |0   |1   |0   |0   |1   |0   |1   |0   |1   |1    |0    |0    |1    |0    |1    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |1    |1    |0    |1    |0    |


##### B) Example infected host VCF (.gz) file, named [infected_50_human_samples_randomized.vcf](./infected_50_human_samples_randomized.vcf):

###### First 5 rows of the genotypes
|#CHROM  |POS|ID |REF|ALT|QUAL|FILTER|INFO|FORMAT|sample1|sample2|sample3|sample4|sample5|sample6|sample7|sample8|sample9|sample10|sample11|sample12|sample13|sample14|sample15|sample16|sample17|sample18|sample19|sample20|sample21|sample22|sample23|sample24|sample25|sample26|sample27|sample28|sample29|sample30|sample31|sample32|sample33|sample34|sample35|sample36|sample37|sample38|sample39|sample40|sample41|sample42|sample43|sample44|sample45|sample46|sample47|sample48|sample49|sample50|
|--------|---|---|---|---|----|------|----|------|-------|-------|-------|-------|-------|-------|-------|-------|-------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|
|1       |92189515|rs9790275|T  |G  |.   |.     |PR  |GT    |0/0    |1/1    |1/1    |1/1    |0/0    |0/0    |0/0    |0/0    |1/1    |1/1     |0/0     |0/1     |0/0     |0/1     |0/1     |0/1     |1/1     |1/0     |0/0     |0/0     |1/0     |0/1     |0/0     |1/1     |0/1     |0/1     |0/0     |0/0     |0/1     |0/0     |0/0     |1/0     |0/0     |0/0     |0/1     |0/1     |1/1     |0/1     |1/1     |0/1     |1/1     |./1     |./0     |./0     |0/1     |0/1     |0/1     |1/1     |1/0     |0/1     |
|1       |115475234|rs7964180|G  |A  |.   |.     |PR  |GT    |1/0    |0/1    |0/1    |1/0    |0/0    |1/0    |0/0    |0/1    |./.    |1/1     |1/1     |1/0     |1/0     |1/0     |1/0     |0/1     |1/1     |1/1     |0/1     |1/0     |0/1     |1/1     |0/0     |1/0     |1/1     |1/0     |1/0     |1/0     |1/0     |0/1     |1/0     |1/1     |0/0     |0/0     |1/0     |1/0     |1/1     |0/0     |1/1     |1/0     |1/0     |./1     |1/0     |0/0     |0/0     |0/0     |0/1     |0/1     |1/1     |1/0     |
|1       |169523766|rs7425082|C  |A  |.   |.     |PR  |GT    |1/0    |0/1    |1/0    |1/1    |1/1    |1/1    |0/0    |0/1    |0/1    |0/1     |0/0     |1/0     |0/0     |0/0     |1/1     |0/1     |0/0     |1/1     |1/0     |0/0     |1/1     |0/0     |1/1     |1/0     |0/1     |0/0     |1/0     |1/0     |0/1     |0/0     |0/1     |1/1     |1/1     |1/1     |1/1     |1/0     |1/1     |1/1     |1/1     |1/0     |1/1     |1/0     |1/0     |1/1     |0/1     |0/1     |1/0     |0/1     |1/0     |0/1     |
|4       |150794750|rs6739305|C  |A  |.   |.     |PR  |GT    |1/0    |0/1    |0/0    |1/0    |1/0    |0/1    |0/0    |0/0    |0/0    |0/0     |1/0     |1/1     |0/1     |1/1     |1/1     |1/1     |1/1     |0/1     |0/1     |1/0     |0/0     |1/0     |0/1     |0/1     |0/1     |1/0     |0/1     |1/0     |0/0     |0/1     |1/1     |1/0     |0/1     |1/1     |0/1     |0/0     |1/1     |0/1     |1/0     |1/1     |1/1     |0/1     |1/1     |1/0     |1/0     |0/0     |0/1     |0/0     |0/0     |0/0     |
|5       |164520833|rs1473244|T  |C  |.   |.     |PR  |GT    |1/1    |1/0    |0/1    |0/0    |0/0    |1/1    |1/0    |0/0    |0/0    |1/0     |1/1     |1/0     |0/1     |1/0     |0/1     |1/0     |1/0     |0/1     |0/1     |0/0     |1/1     |1/1     |1/0     |1/0     |1/1     |0/0     |1/0     |1/0     |1/1     |0/1     |1/1     |1/1     |1/1     |0/0     |1/0     |0/0     |1/1     |0/0     |1/1     |0/1     |1/0     |1/0     |0/0     |1/0     |0/1     |0/1     |1/0     |0/1     |0/1     |1/1     |


##### C) Example healthy host VCF (.gz) file, named [healthy_52_host_samples_randomized.vcf](./healthy_52_host_samples_randomized.vcf):

###### First 5 Rows of the genotypes
|#CHROM  |POS|ID |REF|ALT|QUAL|FILTER|INFO|FORMAT|healthsample1|healthsample2|healthsample3|healthsample4|healthsample5|healthsample6|healthsample7|healthsample8|healthsample9|healthsample10|healthsample11|healthsample12|healthsample13|healthsample14|healthsample15|healthsample16|healthsample17|healthsample18|healthsample19|healthsample20|healthsample21|healthsample22|healthsample23|healthsample24|healthsample25|healthsample26|healthsample27|healthsample28|healthsample29|healthsample30|healthsample31|healthsample32|healthsample33|healthsample34|healthsample35|healthsample36|healthsample37|healthsample38|healthsample39|healthsample40|healthsample41|healthsample42|healthsample43|healthsample44|healthsample45|healthsample46|healthsample47|healthsample48|healthsample49|healthsample50|healthsample51|healthsample52|
|--------|---|---|---|---|----|------|----|------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|
|1       |92189515|rs9790275|T  |G  |.   |.     |PR  |GT    |0/1          |0/0          |0/0          |0/1          |0/1          |1/1          |1/1          |1/1          |0/1          |1/1           |1/1           |1/0           |1/1           |1/0           |1/0           |0/0           |0/1           |1/1           |1/0           |1/0           |0/1           |1/0           |1/0           |0/1           |0/1           |1/1           |0/1           |1/1           |1/1           |0/1           |0/0           |0/0           |0/0           |0/1           |0/0           |1/1           |0/1           |0/0           |1/1           |1/1           |0/1           |1/0           |1/1           |1/1           |0/1           |1/1           |0/0           |0/1           |0/1           |0/0           |0/1           |1/0           |
|1       |115475234|rs7964180|G  |A  |.   |.     |PR  |GT    |1/0          |1/1          |0/1          |1/0          |1/0          |1/0          |0/1          |1/1          |1/0          |0/0           |0/0           |1/0           |1/0           |0/1           |0/1           |0/1           |1/0           |1/1           |1/1           |0/1           |1/1           |0/0           |1/0           |0/0           |0/1           |1/0           |1/0           |0/0           |0/1           |0/1           |1/1           |1/1           |0/0           |1/1           |1/1           |0/0           |1/0           |1/1           |1/0           |0/0           |1/1           |0/1           |1/1           |1/0           |1/1           |1/1           |0/0           |1/1           |0/1           |0/0           |1/1           |0/1           |
|1       |169523766|rs7425082|C  |A  |.   |.     |PR  |GT    |1/0          |0/1          |1/0          |1/1          |1/1          |1/1          |0/0          |0/1          |0/1          |0/1           |0/0           |1/0           |0/0           |0/0           |1/0           |1/0           |0/0           |1/1           |1/1           |0/1           |0/0           |1/1           |1/0           |0/0           |0/0           |1/1           |1/1           |0/1           |1/0           |1/1           |1/1           |1/1           |0/1           |1/0           |1/0           |0/0           |0/1           |1/1           |1/1           |0/1           |1/0           |0/0           |0/1           |0/1           |0/1           |1/0           |0/0           |1/1           |1/0           |1/0           |0/0           |0/1           |
|4       |150794750|rs6739305|C  |A  |.   |.     |PR  |GT    |1/0          |0/1          |0/0          |1/0          |1/0          |0/1          |0/0          |0/0          |0/0          |0/0           |1/0           |1/1           |0/1           |1/1           |0/1           |0/1           |1/0           |0/1           |1/1           |0/1           |0/0           |0/0           |0/0           |0/0           |1/0           |0/0           |1/0           |1/0           |1/0           |1/1           |1/0           |0/0           |0/0           |1/1           |1/1           |1/0           |1/0           |1/1           |1/1           |0/1           |0/0           |1/0           |0/1           |0/1           |0/1           |1/1           |0/1           |0/0           |0/0           |0/1           |1/1           |0/0           |
|5       |164520833|rs1473244|T  |C  |.   |.     |PR  |GT    |1/1          |1/0          |1/0          |0/0          |1/0          |1/1          |1/0          |0/0          |0/0          |1/0           |1/1           |1/0           |0/1           |1/0           |0/0           |1/0           |0/1           |0/0           |1/1           |1/0           |0/1           |1/0           |0/1           |0/0           |0/0           |0/1           |1/0           |0/0           |0/1           |0/1           |0/0           |0/1           |0/0           |0/1           |0/0           |0/1           |0/1           |1/1           |0/0           |0/1           |1/1           |1/1           |1/0           |0/1           |0/1           |1/1           |0/0           |1/0           |1/1           |1/1           |1/0           |1/0           |


### Workflow Step 1: Extracting Allele Frequencies from the Healthy Host Dataset
The initial step involves extracting allele frequencies from the VCF (Variant Call Format) file of the healthy host. To accomplish this, we utilize PLINK2, a powerful tool for genetic data analysis. The following command with PLINK2 will generate a frequency file, which includes important columns like `#CHROM`, `POS`, `FREQS`, and `OBS_CT`. These columns represent chromosomal location, position, allele frequencies, and observation count in the healthy host sample, respectively.

**Command in PLINK2:**

To execute this step, use the following command in your terminal or script:

```bash
./plink2 --vcf [UNINFECTED_GENOTYPE_FILE] --freq cols=chrom,pos,ref,alt,freq,nobs --out [OUTPUT_FREQ_FILE]
```

Replace `[UNINFECTED_GENOTYPE_FILE]` with the path to your healthy host VCF file and `[OUTPUT_FREQ_FILE]` with the desired output file name.

#### Example Bash Script:

Here’s an example of how the command looks with actual file names:

```bash
./plink2 --vcf ./healthy_52_host_samples_randomized.vcf --freq cols=chrom,pos,ref,alt,freq,nobs --out healthy_50_host_randomized_freq
```

#### Example Output Frequency File from PLINK2:

The output file `healthy_50_host_randomized_freq.afreq` will have a tabular format with the following columns. Displayed below are the first 5 rows as an example:

|#CHROM  |POS|ID |REF|ALT|FREQS|OBS_CT|
|--------|---|---|---|---|-----|------|
|1       |92189515|rs9790275|T  |G  |0.442308,0.557692|104   |
|1       |115475234|rs7964180|G  |A  |0.442308,0.557692|104   |
|1       |169523766|rs7425082|C  |A  |0.480769,0.519231|104   |
|4       |150794750|rs6739305|C  |A  |0.557692,0.442308|104   |
|5       |164520833|rs1473244|T  |C  |0.528846,0.471154|104   |


### Workflow Step 2: Calculating Allele Frequencies and Incorporating Healthy Host Data

In this step, we use the [`EstimateandCombineFrequencies.py`](./EstimateandCombineFrequencies.py) script to process VCF (Variant Call Format) files. This script calculates allele frequencies, merges these with additional frequency data from healthy hosts, and outputs a file for each pathogen Single Amino Acid Polymorphism (SAAP). It's designed for efficient parallel processing, handling multiple pathogens simultaneously. You can run the script for all SAAPs or target specific ones.

#### Using the Shell Script

The [`run_EstimateandCombineFrequencies.sh`](./run_EstimateandCombineFrequencies.sh) script automates the execution of [`EstimateandCombineFrequencies.py`](./EstimateandCombineFrequencies.py). Below is the usage example and the command structure within the script:


```bash
python EstimateandCombineFrequencies.py <vcf_file> <pathogen_file> <healthy_afreq_file> <output_directory> [specified_pathogens] [number_of_cpus]
```

- `<vcf_file>`: Path to the VCF file.
- `<pathogen_file>`: Path to the pathogen data file.
- `<healthy_afreq_file>`: Path to the .afreq frequency data file.
- `<output_directory>`: Path to the directory where output files will be saved.
- `[specified_pathogens]`: (Optional) A comma-separated list of specific pathogens to process. If omitted, all pathogens will be processed.
- `[number_of_cpus]`: (Optional) Number of CPUs to use for parallel processing. Default is 1.

#### Example Bash Script:

Here are examples of how to run the script for all SAAPs and for specific SAAPs:

```bash
#!/bin/bash

#To run the script over all SAAP columns
python EstimateandCombineFrequencies.py ./infected_50_human_samples_randomized.vcf ../Pathogen_pre_GWAs_analysis/output_example_50_pathogen_samples_SAAP.txt ./healthy_50_host_randomized_freq.afreq /mnt/nvme1n1p1/co_GWAS/cogenomics_method/Tutorial/Indices_calc/new_combined "" 4

#To run the script over specified columns in the pathogen file
ppython EstimateandCombineFrequencies.py ./infected_50_human_samples_randomized.vcf ../Pathogen_pre_GWAs_analysis/output_example_50_pathogen_samples_SAAP.txt ./healthy_50_host_randomized_freq.afreq /mnt/nvme1n1p1/co_GWAS/cogenomics_method/Tutorial/Indices_calc/new_combined "V1_M,V12_X,V18_Q" 4
```

##### Output Example

Running the [run_EstimateandCombineFrequencies.sh](./run_EstimateandCombineFrequencies.sh) script generates `.tsv` files for each SAAP column from the [output_example_50_pathogen_samples_SAAP.txt](../Pathogen_pre-GWAs_analysis/output_example_50_pathogen_samples_SAAP.txt) file. For each SAAP, it produces a file with detailed allele frequency and count data. A column named `swapped` indicates if an adjustment was necessary due to mismatches in the reference (REF) and alternative (ALT) alleles in the infected and uninfected genotype files. This is marked as `yes` or `no`.

Below is a snapshot of an example output file (e.g., `output_V1_M.tsv`):


|CHROM   |POS|ID |REF_Host1|ALT_Host2|PARASITE1_HOST1_AF|PARASITE1_HOST2_AF|PARASITE2_HOST1_AF|PARASITE2_HOST2_AF|PARASITE1_HOST1_COUNT|PARASITE1_HOST2_COUNT|PARASITE2_HOST1_COUNT|PARASITE2_HOST2_COUNT|OBS_CT_INF|OBS_CT_UNINF|UNINF_HOST1_FREQ|UNINF_HOST2_FREQ|swapped|
|--------|---|---|---------|---------|------------------|------------------|------------------|------------------|---------------------|---------------------|---------------------|---------------------|----------|------------|----------------|----------------|-------|
|1       |92189515|rs9790275|T        |G        |0.58333           |0.41667           |0.52174           |0.47826           |28                   |20                   |24                   |22                   |94        |104         |0.442308        |0.557692        |no     |
|1       |115475234|rs7964180|G        |A        |0.52083           |0.47917           |0.45833           |0.54167           |25                   |23                   |22                   |26                   |96        |104         |0.442308        |0.557692        |no     |
|1       |169523766|rs7425082|C        |A        |0.42308           |0.57692           |0.4375            |0.5625            |22                   |30                   |21                   |27                   |100       |104         |0.480769        |0.519231        |no     |
|4       |150794750|rs6739305|C        |A        |0.51923           |0.48077           |0.5               |0.5               |27                   |25                   |24                   |24                   |100       |104         |0.557692        |0.442308        |no     |
|5       |164520833|rs1473244|T        |C        |0.48077           |0.51923           |0.45833           |0.54167           |25                   |27                   |22                   |26                   |100       |104         |0.528846        |0.471154        |no     |


This table includes various columns such as:

- `CHROM`: Represents the chromosome code.
- `POS`: Indicates the base-pair coordinate.
- `ID`: Represents the Variant ID, which is optional. SNPs do not require individual IDs.
- `REF_Host1`: The reference allele, which is associated with the host of type 1.
- `ALT_Host2`: The alternative allele, associated with the host of type 2.
- `PARASITE1_HOST1_AF`: The allele frequency of the alternative allele (REF_Host1) infected with parasite type 1 in the infected subpopulation. Calculated as the number of reference alleles divided by the total number of alleles observed as parasite type 1.
- `PARASITE1_HOST2_AF`: The allele frequency of the alternative allele (ALT_Host2) infected with parasite type 1 in the infected subpopulation. Calculated as the number of alternative alleles divided by the total number of alleles observed as parasite type 1.
- `PARASITE2_HOST1_AF`: The allele frequency of the alternative allele (REF_Host1) infected with parasite type 2 in the infected subpopulation. Calculated as the number of reference alleles divided by the total number of alleles observed as parasite type 2.
- `PARASITE2_HOST2_AF`: The allele frequency of the alternative allele (ALT_Host2) infected with parasite type 2 in the infected subpopulation. Calculated as the number of alternative alleles divided by the total number of alleles observed as parasite type 2.
- `PARASITE1_HOST1_COUNT`: Denotes the allele count of hosts with genotype 1 infected by pathogens genotype 1 in the infected subpopulation.
- `PARASITE1_HOST2_COUNT`: Denotes the allele count of hosts with genotype 2 infected by pathogens genotype 1 in the infected subpopulation.
- `PARASITE2_HOST1_COUNT`: Denotes the allele count of hosts with genotype 1 infected by pathogens genotype 2 in the infected subpopulation.
- `PARASITE2_HOST2_COUNT`: Denotes the allele count of hosts with genotype 2 infected by pathogens genotype 2 in the infected subpopulation.
- `TOTAL_ALLELES_OBSERVED`: The total count of alleles observed, which includes both reference and alternative alleles from both host types.
- `OBS_CT_INF`: Represents the count of alleles in the infected subpopulation. 
- `OBS_CT_UNINF`: Denotes the number of alleles in the uninfected subpopulation.
- `UNINF_HOST1_FREQ`: Specifies the frequency of uninfected hosts of type 1 within the uninfected subpopulation.
- `UNINF_HOST2_FREQ`: Indicates the frequency of uninfected hosts of type 2 within the uninfected subpopulation.
- `swapped`: Indicates whether adjustment was necessary due to mismatches between the REF (host of type 1) and ALT (host of type 2) in the infected and uninfected genotype files, expressed as `yes` or `no`.

The output files, such as `output_V1_M.tsv`, `output_V2_S.tsv`, etc. can be found in the `Tutorial_FinalOutput` folder.


### Workflow Step 3: Calculating Indices and Updating the .tsv File

This step involves using the [`calculate_indices.py`](./calculate_indices.py) script, which is executed through the `process_files_indices_calc.sh` shell script. The script calculates various indices, including CSA, HS, HP, and PI, and appends them to the existing .tsv files.

##### Example Bash Script

To understand how to use the script, let's examine `process_files_indices_calc.sh`:

```bash
#!/bin/bash

DIRECTORY=$1

for file in "$DIRECTORY"*.tsv
do
    python calculate_indices.py "$file"
done

```
This script iterates through all .tsv files in a specified directory and runs the [`calculate_indices.py`](./calculate_indices.py) script on each file.

**Execution Command:**

To run the script, use the following command in your terminal:

```bash
bash process_files_indices_calc.sh ./Tutorial_FinalOutput/
```
This command executes the script for all .tsv files located in the `./Tutorial_FinalOutput/` directory.

##### Example Output

The execution of `process_files_indices_calc.sh` updates each `.tsv` file with new columns: CSA, HS, PI, and HP. These columns represent calculated index values for each set of allele frequencies. Below is an example of the updated `.tsv` file for a specific SAAP (`V1_M`):

|CHROM   |POS|ID |REF_Host1|ALT_Host2|PARASITE1_HOST1_AF|PARASITE1_HOST2_AF|PARASITE2_HOST1_AF|PARASITE2_HOST2_AF|PARASITE1_HOST1_COUNT|PARASITE1_HOST2_COUNT|PARASITE2_HOST1_COUNT|PARASITE2_HOST2_COUNT|OBS_CT_INF|OBS_CT_UNINF|UNINF_HOST1_FREQ|UNINF_HOST2_FREQ|swapped|CSA|HS |PI |HP |
|--------|---|---|---------|---------|------------------|------------------|------------------|------------------|---------------------|---------------------|---------------------|---------------------|----------|------------|----------------|----------------|-------|---|---|---|---|
|1       |92189515|rs9790275|T        |G        |0.58333           |0.41667           |0.52174           |0.47826           |28                   |20                   |24                   |22                   |94        |104         |0.442308        |0.557692        |no     |0.0619317079066297|0.00467019348620334|0.0143380507850013|0.00203351555965899|
|1       |115475234|rs7964180|G        |A        |0.52083           |0.47917           |0.45833           |0.54167           |25                   |23                   |22                   |26                   |96        |104         |0.442308        |0.557692        |no     |0.062513567784812|0.00193031840623788|0.00127598148847744|0.000891545279085698|
|1       |169523766|rs7425082|C        |A        |0.42308           |0.57692           |0.4375            |0.5625            |22                   |30                   |21                   |27                   |100       |104         |0.480769        |0.519231        |no     |0.0145548775017339|0.00202996393245526|0.0371854174236915|0.0014071357797891|
|4       |150794750|rs6739305|C        |A        |0.51923           |0.48077           |0.5               |0.5               |27                   |25                   |24                   |24                   |100       |104         |0.557692        |0.442308        |no     |0.0192192226856089|0.00187091187618987|0.0388367599896831|0.00130511441922987|
|5       |164520833|rs1473244|T        |C        |0.48077           |0.51923           |0.45833           |0.54167           |25                   |27                   |22                   |26                   |100       |104         |0.528846        |0.471154        |no     |0.0224584031065535|0.00231547512976453|0.0405284876398985|0.00153986768998361|

In this updated table, you will find:

- Columns `CSA`, `HS`, `PI`, `HP`: These columns contain the calculated indices for each allele frequency set.
  - `CSA`: Represents association between the genotype of infected hosts and the genotype of the respective infecting pathogen strains.
  - `HS`: Compares allele frequencies in the infected versus non-infected host subsamples.
  - `PI`: Assesses the difference between pathogen allele frequencies.
  - `HP`: Reflects the difference of allele frequencies of one host genotype infected by one pathogen allele and when non-infected.

The final output files, with names like `*_indices_calc.tsv`, include these new columns and can be found in the `Tutorial_FinalOutput` folder.
    
## Notes

- Ensure that the sample IDs are consistent and in the same order across all files.
- Check that the pathogen files are properly formatted with binary pathogens for each pathogen.
