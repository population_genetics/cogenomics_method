import pandas as pd
import numpy as np
import sys
import os

def calculate_indices(row):
    # Define the f values according to the relationships
    f_11 = row["PARASITE1_HOST1_COUNT"] / (row["OBS_CT_UNINF"] + row["OBS_CT_INF"])
    f_12 = row["PARASITE2_HOST1_COUNT"] / (row["OBS_CT_UNINF"] + row["OBS_CT_INF"])
    f_21 = row["PARASITE1_HOST2_COUNT"] / (row["OBS_CT_UNINF"] + row["OBS_CT_INF"])
    f_22 = row["PARASITE2_HOST2_COUNT"] / (row["OBS_CT_UNINF"] + row["OBS_CT_INF"])
    
    f_tilde_11 = row["PARASITE1_HOST1_COUNT"] / row["OBS_CT_INF"]
    f_tilde_12 = row["PARASITE2_HOST1_COUNT"] / row["OBS_CT_INF"]
    f_tilde_21 = row["PARASITE1_HOST2_COUNT"] / row["OBS_CT_INF"]
    f_tilde_22 = row["PARASITE2_HOST2_COUNT"] / row["OBS_CT_INF"]
    
    f_1z = row["UNINF_HOST1_FREQ"] / (row["OBS_CT_UNINF"] + row["OBS_CT_INF"])
    f_2z = row["UNINF_HOST2_FREQ"] / (row["OBS_CT_UNINF"] + row["OBS_CT_INF"])
    
    # Calculate the indices
    f_bar_1 = np.sqrt((f_tilde_11 + f_tilde_12) * (f_tilde_21 + f_tilde_22) * 
                      (f_tilde_11 + f_tilde_21) * (f_tilde_12 + f_tilde_22))
    
    f_bar_2 = (f_11 + f_12 + f_1z) * (f_21 + f_22 + f_2z)
    
    CSA = np.abs((f_tilde_11 * f_tilde_22 - f_tilde_12 * f_tilde_21) / f_bar_1)
    HS = np.abs(((f_11 + f_12) * f_2z - (f_21 + f_22) * f_1z) / f_bar_2)
    PI = np.abs((f_12 * f_22 - f_11 * f_21) / f_bar_2)
    HP = np.abs((f_12 * f_2z - f_21 * f_1z) / f_bar_2)
    
    return CSA, HS, PI, HP

def process_file(file_path):
    # Load the data
    data = pd.read_csv(file_path, sep='\t')
    
    # Calculate the indices and add them as new columns
    data['CSA'], data['HS'], data['PI'], data['HP'] = zip(*data.apply(calculate_indices, axis=1))
    
    # Save the modified data to a new file
    new_file_path = os.path.splitext(file_path)[0] + '_indices_calc.tsv'
    data.to_csv(new_file_path, sep='\t', index=False)

if __name__ == "__main__":
    # Example usage: python process_files.py file1.tsv file2.tsv file3.tsv
    for file_path in sys.argv[1:]:
        process_file(file_path)

