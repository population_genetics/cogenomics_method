#!/bin/bash

# Check if correct number of arguments is provided
if [ "$#" -ne 4 ]; then
    echo "Usage: $0 <input_file_path> <maf_threshold> <output_file_path> <run_optional>"
    exit 1
fi


# Get the directory where the script is located
script_dir="$(dirname "$0")"

# Optional arguments
input_file_path="$1"
maf_threshold="$2"
output_file_path="$3"
run_optional="$4"

# Run R Script
Rscript "${script_dir}/process_fasta.R" "$input_file_path" "$maf_threshold" "$output_file_path" "$run_optional"

