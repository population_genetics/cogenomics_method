# 1.) Pathogen pre-GWAs analysis
# Fasta File Processing and Analysis

## Overview

This directory contains scripts designed for the processing and analysis of FASTA files, particularly those containing virus data with protein sequences. The scripts' main function is to transform an alignment into a matrix indicating the presence or absence of proteins (Single Amino Acid Polymorphism - SAAP). This transformation aids in associating bi-allelic sites in pathogens with those in hosts. Additionally, the scripts facilitate various operations on protein sequences and conduct Minor Allele Frequency (MAF) filtering. The scripts are designed to incorporate parameters directly from the command line. For more detailed information refer to the `Pathogen_pre_processing_markdown.html` or `Pathogen_pre_processing_markdown.pdf`. It explains each step of the process for converting a (virus) protein alignment into SAAPs.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Repository Contents](#repository-contents)
- [Usage](#usage)
  - [Running the Script](#running-the-script)
- [Tutorial](#tutorial)
- [Additional Information](#additional-information)
- [Support & Contribution](#support--contribution)
- [License](#license)

## Prerequisites

- **Programming Language:** R
- **R Libraries:** 
  - `tibble`
  - `microseq`
  - `dplyr`
  - `stringr`
  - `tidyr`
  - `ape`
  - `adegenet`
- **Shell:** Bash (Unix shell)

## Repository Contents

- `process_fasta.R`: R-script processes and analyzes Fasta files.
- `run_process_fasta.sh`: Bash script to execute `process_fasta.R` from the command line.
- `example_log_output.log`: Sample Bash output log with alignment snippets.
- `example_dataset_pathogen.fasta`: Sample FASTA file with 13 pathogen sequences (50 amino acids each).
- `output_example_pathogen_SAAP.txt`: Sample output file with binary Single Amino Acid Polymorphisms (SAAPs).
- `Pathogen_pre_processing_markdown.html`: HTML guide for converting protein alignments into binary.
- `Pathogen_pre_processing_markdown.pdf`: PDF version of the above guide.
- `README.md`: Documentation file.

## Usage

### Running the Script

Execute the script using the following Bash command:

```bash
bash run_process_fasta.sh [INPUT_FILE_PATH] [MAF_THRESHOLD] [OUTPUT_FILE_PATH] [RUN_OPTIONAL]

    [INPUT_FILE_PATH]: Path to the input Fasta file, including the file name.
    [MAF_THRESHOLD]: Minor Allele Frequency (MAF) threshold for filtering, provided as a decimal (decimal format, e.g., 0.02 for 2%).
    [OUTPUT_FILE_PATH]: Path where the processed data will be saved.
    [RUN_OPTIONAL]: If TRUE, gaps (missing sites in the alignment) will be filled with sequences from individuals with minimal distance; if FALSE, this step is skipped.
    
    Example Usage:
    
    bash run_process_fasta.sh example.fasta 0.02 output.txt TRUE
```

## Tutorial 
An example `.fasta` file with random amino acid sequences is provided for demonstration purposes.

### Step 1: Inspecting a Fasta File

A sample file contains 50 pathogen samples, each with 50 amino acids. The file format is as follows:

```text
>sample1
MXTLPKPQRKTXRNTVRRPLDIKFPGGGQIVGGVYVLPRRGPRLGVCATR
>sample2
VSTLPKPQRKTXPNTVRYPLDVKFPGGGQIVGGVYVLPRRGPRLGVCATG
>sample3
MSTLPKPQRQTKRNTIRYPQDVKFPGGGQIVSGVYVLPRFGPRLGVRATR
>sample4
VTRLPKPQRKTXRNTVRRPQDVKFPGGGQIVSGVYVLPRFGPRLGVRATR
>sample5
MSRLPTPQRKTXYNTIRRPQDVKFPGGGQIVGGVYVLPRFGPRLGVCATG
>sample6
VSRLPTPQRKTXYNTIRRPLDVKFPGGGQIVGGVYVLPCFGPRLGVRATR
>sample7
MSTLPKPQRKTKYNTVCRPQDVKFPGGGQIVGGVYVSPCFGPRLGVRATR
>sample8
VSTLPKPQRKTKYNTIRQPLDVKFPGGGQIVXGVYVLPCRGPRLGVRATR
>sample9
MTRLPTPQRKTKPNTIRQPQDVKFPGGGQIVGGVYVSPRRGPRLGVCATR
>sample10
MTTLGTPQRKTKPNTVRQPQDVKFPGGGQIVXGVVVLPRRGPRLGVCATR
>sample11
MTTLGKPQRKTKPNTVRQPQDVKFPGGGQIVXGVVVLPRRGPRLGVCATG
>sample12
MTYLGKPQRKTXPNTVRYPQDVKFPGGGQIVGGVVVLPRRGPRLGVCATG
>sample13
VSYLPKPQRKTXYNTIRQPLDVKFPGGGQIVSGVVVSPRRGPRLGVRATR
......
```

### Step 2: Converting to SAAP Format
Use this command to convert the `example_dataset_pathogen.fasta` into a file showing presence/absence of each amino acid (SAAP):


```bash
bash ./run_process_fasta.sh ./example_dataset_pathogen.fasta 0.02 ./output_example_pathogen_SAAP.txt TRUE
```

The command logs each step to the terminal, with an example log in `example_log_output.log`.

### Step 3: Output File Format

The resulting `output_example_pathogen_SAAP.txt` will have the following format:

|ID      |V1_M|V2_X|V2_S|V2_T|V3_T|V3_R|V3_Y|V5_P|V6_K|V7_P|V12_X|V13_R|V13_P|V13_Y|V16_V|V17_R|V18_R|V18_Y|V18_Q|V20_Q|V22_V|V32_G|V32_S|V32_X|V34_V|V35_Y|V36_V|V37_L|V39_R|V40_R|V47_R|V50_R|
|--------|----|----|----|----|----|----|----|----|----|----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|sample1 |1   |1   |0   |0   |1   |0   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |1    |1    |1    |1    |1    |
|sample2 |0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |1    |0    |1    |0    |0    |1    |1    |1    |1    |1    |1    |1    |0    |
|sample3 |1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |1    |0    |0    |0    |1    |0    |1    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample4 |0   |0   |0   |1   |0   |1   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample5 |1   |0   |1   |0   |0   |1   |0   |1   |0   |1   |1    |0    |0    |1    |0    |1    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |1    |1    |0    |1    |0    |
|sample6 |0   |0   |1   |0   |0   |1   |0   |1   |0   |1   |1    |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |1    |0    |0    |1    |1    |1    |1    |0    |0    |0    |1    |
|sample7 |1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |1    |0    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |1    |
|sample8 |0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |0    |1    |0    |0    |1    |1    |0    |0    |0    |1    |1    |1    |1    |1    |0    |1    |0    |1    |
|sample9 |1   |0   |0   |1   |0   |1   |0   |1   |0   |1   |0    |0    |1    |0    |0    |1    |0    |0    |1    |0    |0    |1    |0    |0    |1    |1    |1    |0    |1    |1    |1    |1    |
|sample10|1   |0   |0   |1   |1   |0   |0   |0   |0   |1   |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |0    |0    |0    |1    |1    |0    |1    |1    |1    |1    |1    |1    |
|sample11|1   |0   |0   |1   |1   |0   |0   |0   |1   |1   |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |0    |0    |0    |1    |1    |0    |1    |1    |1    |1    |1    |0    |
|sample12|1   |0   |0   |1   |0   |0   |1   |0   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |0    |0    |1    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |
|sample13|0   |0   |1   |0   |0   |0   |1   |1   |1   |1   |1    |0    |0    |1    |0    |1    |0    |0    |1    |1    |0    |0    |1    |0    |1    |0    |1    |0    |1    |1    |0    |1    |
|sample14|0   |0   |1   |0   |1   |0   |0   |0   |1   |1   |0    |0    |1    |0    |1    |1    |0    |1    |0    |1    |0    |1    |0    |0    |1    |1    |0    |1    |1    |1    |1    |0    |
|sample15|1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |0    |0    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |1    |
|sample16|1   |1   |0   |0   |1   |0   |0   |0   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |0    |1    |0    |1    |1    |1    |1    |
|sample17|0   |0   |0   |1   |0   |0   |1   |0   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |0    |0    |1    |0    |0    |1    |0    |0    |1    |1    |0    |1    |0    |
|sample18|1   |0   |0   |1   |1   |0   |0   |0   |1   |1   |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |1    |1    |0    |
|sample19|0   |0   |0   |1   |0   |1   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |0    |0    |1    |1    |1    |1    |0    |1    |0    |0    |1    |
|sample20|1   |0   |1   |0   |1   |0   |0   |1   |1   |0   |0    |0    |0    |1    |1    |0    |0    |1    |0    |0    |0    |1    |0    |0    |1    |1    |0    |0    |0    |0    |0    |1    |
|sample21|0   |0   |1   |0   |0   |0   |1   |0   |1   |1   |1    |0    |0    |1    |0    |0    |0    |0    |1    |1    |0    |0    |1    |0    |1    |0    |1    |0    |1    |1    |0    |1    |
|sample22|0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |0    |1    |0    |0    |1    |1    |0    |0    |0    |1    |1    |1    |1    |1    |0    |1    |0    |1    |
|sample23|0   |0   |1   |0   |1   |0   |0   |1   |1   |0   |1    |0    |1    |0    |1    |1    |0    |1    |0    |1    |0    |1    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |
|sample24|0   |0   |1   |0   |0   |0   |1   |1   |0   |0   |1    |0    |0    |1    |0    |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |1    |0    |0    |0    |1    |
|sample25|0   |1   |0   |0   |1   |0   |0   |0   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |1    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |
|sample26|0   |0   |0   |1   |0   |1   |0   |1   |1   |1   |1    |1    |0    |0    |0    |1    |1    |0    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample27|0   |0   |1   |0   |0   |0   |1   |1   |1   |1   |1    |0    |0    |1    |0    |1    |0    |0    |1    |1    |0    |0    |1    |0    |1    |0    |1    |0    |1    |1    |0    |0    |
|sample28|1   |0   |0   |1   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |0    |0    |0    |1    |0    |0    |0    |1    |0    |0    |0    |1    |0    |0    |0    |0    |0    |1    |
|sample29|1   |1   |0   |0   |0   |0   |1   |0   |1   |1   |1    |1    |0    |0    |1    |0    |1    |0    |0    |1    |1    |1    |0    |0    |1    |0    |0    |1    |1    |1    |1    |1    |
|sample30|1   |0   |0   |1   |0   |0   |1   |0   |1   |0   |1    |0    |1    |0    |1    |1    |0    |1    |0    |0    |0    |1    |0    |0    |0    |0    |1    |1    |1    |1    |1    |0    |
|sample31|1   |0   |1   |0   |0   |1   |0   |1   |0   |0   |1    |0    |0    |1    |0    |1    |1    |0    |0    |0    |0    |0    |0    |1    |1    |1    |1    |1    |1    |0    |1    |0    |
|sample32|0   |0   |0   |1   |0   |1   |0   |0   |0   |1   |1    |0    |0    |1    |1    |1    |1    |0    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample33|0   |0   |1   |0   |0   |0   |1   |1   |1   |1   |1    |0    |0    |1    |1    |1    |0    |0    |1    |1    |0    |0    |1    |0    |1    |0    |1    |0    |1    |1    |0    |1    |
|sample34|1   |0   |0   |1   |1   |0   |0   |0   |1   |1   |0    |1    |0    |0    |1    |1    |0    |0    |1    |0    |0    |0    |0    |1    |1    |0    |1    |1    |1    |1    |1    |0    |
|sample35|0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |1    |1    |1    |0    |0    |1    |0    |0    |0    |1    |1    |1    |1    |1    |0    |1    |0    |1    |
|sample36|0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |1    |0    |1    |0    |0    |1    |1    |1    |1    |1    |1    |1    |1    |
|sample37|1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |1    |0    |0    |0    |1    |0    |1    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample38|1   |0   |1   |0   |0   |1   |0   |1   |0   |1   |1    |0    |0    |1    |0    |1    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |1    |1    |0    |1    |1    |
|sample39|1   |0   |0   |1   |0   |0   |1   |0   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |0    |0    |1    |0    |0    |1    |0    |1    |1    |1    |1    |1    |1    |
|sample40|0   |0   |1   |0   |0   |1   |0   |1   |0   |1   |1    |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |1    |0    |0    |1    |1    |1    |1    |0    |0    |0    |1    |
|sample41|0   |0   |0   |1   |0   |1   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample42|1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |1    |0    |1    |0    |0    |0    |0    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |1    |
|sample43|0   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |0    |0    |1    |0    |1    |0    |0    |1    |1    |0    |0    |0    |1    |1    |1    |1    |1    |0    |1    |0    |1    |
|sample44|1   |0   |0   |1   |0   |1   |0   |1   |0   |1   |0    |0    |1    |0    |0    |1    |0    |0    |1    |0    |0    |1    |0    |0    |1    |1    |1    |0    |1    |1    |1    |1    |
|sample45|1   |0   |0   |1   |1   |0   |0   |0   |0   |1   |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |0    |0    |0    |1    |1    |0    |1    |1    |1    |1    |1    |1    |
|sample46|1   |0   |0   |1   |1   |0   |0   |0   |1   |1   |0    |0    |1    |0    |1    |1    |0    |0    |1    |0    |0    |0    |0    |1    |1    |0    |1    |1    |1    |1    |1    |0    |
|sample47|1   |0   |0   |1   |0   |0   |1   |0   |1   |1   |1    |0    |1    |0    |1    |1    |0    |1    |0    |0    |0    |1    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |
|sample48|0   |0   |1   |0   |0   |0   |1   |1   |1   |1   |1    |0    |0    |1    |0    |1    |0    |0    |1    |1    |0    |0    |1    |0    |1    |0    |1    |0    |1    |1    |0    |1    |
|sample49|1   |0   |1   |0   |1   |0   |0   |1   |1   |1   |0    |1    |0    |0    |0    |1    |0    |1    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |
|sample50|0   |0   |0   |1   |0   |1   |0   |1   |1   |1   |1    |1    |0    |0    |1    |1    |1    |0    |0    |0    |0    |0    |1    |0    |1    |1    |1    |1    |1    |0    |0    |1    |

The column names indicate specific positions in the pathogen genome, each marked with an underscore, to indicate whether a particular amino acid is present or absent. In this context, `0` signifies absence and `1` signifies presence. So for example, at position `2` in the pathogen genome, we find the amino acid `X`, `S`, `T`, therefore we created a three columns at `position 2` with presence and absence for each of them.
Some SAAPs were not included in the `output_example_pathogen_SAAP.txt` file. This exclusion primarily results from the MAF (Minor Allele Frequency) criteria specified in `run_process_fasta.sh`. Sites with a MAF lower than the set threshold were filtered out, leading to the omission of sites that are completely or almost monomorphic.

## Additional Information

    Ensure the provided paths exist and the input file is available at the specified path.
    Ensure the output directory exists; the script does not create new directories.
    Ensure R and the necessary libraries are installed.

	
