#$-cwd
#$-l vf=16G
#$-pe serial 18

#$-N co_GWAs_002_europ

## run the actual job

./plink2 --pfile ./European_pop_hwe_maf_maj_all --glm no-x-sex hide-covar firth-fallback cols=+a1countcc,+a1countcc,+totallelecc,+totallelecc,+a1freq,+a1freqcc,+a1freqcc --pheno ./European_pop_option1_002_pheno_nov_21.txt --1 --covar ./covariates_two_column_europ.txt --covar-variance-standardize --adjust cols=+pos --threads 18
