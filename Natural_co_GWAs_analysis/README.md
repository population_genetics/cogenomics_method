# 3.) Natural co-GWAs Analysis with PLINK2

This section of the repository contains scripts and files necessary to conduct a natural co-Genome-Wide Association Study (co-GWAs) using [PLINK2](https://www.cog-genomics.org/plink/2.0/). The analysis utilizes output data from the [Pathogen_pre_GWAs_analysis](../Pathogen_pre_GWAs_analysis/) and integrates additional covariates into the glm analysis. This process is tailored for an SGE computing cluster and is executed via Bash. The final output includes a GWAs output file for each column in the phenotype file. 
For a comprehensive understanding of GWAs in PLINK2, we recommend reviewing the [PLINK2 documentation](https://www.cog-genomics.org/plink/2.0/). Additionally, for insights into natural co-GWAs analysis, see Ansari et al. (2017) Nature Genetics.

## Table of Contents

- [Repository Contents](#repository-contents)
- [Usage](#usage)
  - [Prerequisites](#prerequisites)
  - [Running the Script](#running-the-script)

## Repository Contents

- `European_pop_option1_002_pheno_nov_21.txt`: This phenotype file is an output from the [Pathogen_pre_GWAs_analysis](../Pathogen_pre_GWAs_analysis/) and serves as an input for the co-GWAs with PLINK2.
- `co_GWAs_002_europ.sh`: A Bash script to execute the co-GWAs analysis in PLINK2 from the command line.
- `covariates_two_column_europ.txt`: A covariate file for the co-GWAs in PLINK2, providing additional data for the analysis.

## Usage

### Prerequisites

Running PLINK2 for the co-GWAs requires six file types as input:
- **PLINK2 Binary Fileset: <prefix>.pgen, <prefix>.psam, <prefix>.pvar**, used with the `--pfile <prefix>` option. PLINK2 binary fileset for the host samples.
- **Covariate File**: Contains all sample IDs (consistent across host and pathogen) and covariates, used with `--covar`.
- **Phenotype File**: Output from [Pathogen_pre_GWAs_analysis](../Pathogen_pre_GWAs_analysis/), detailing pathogen IDs and protein presence/absence in binary format.

### Running the Script

Before running the script, ensure PLINK2 is installed and the required files are correctly placed. Execute the following command in a Bash environment:

```bash
bash co_GWAs_002_europ.sh
```
	
