
# Numerical Estimation of Infection Matrix

## Overview

This folder offers a Python script designed to perform numerical estimations of the infection matrix for each variant association, derived from our co-evolution model (which we provide as an fast inference of the infection matrix). Specifically developed to provide accurate numerical estimates of the alpha values when the disease encounter rate is notably high (>50%), the pipeline allows for direct estimation from data frequencies. Upon analyzing the data, we compute four key indices: CSA, HS, PI, and HP. Given the known disease prevalence $\phi$ and allele frequencies $h_i$ and $p_i$, the undetermined elements in the generalized infection matrix are the coefficients of the GxG matrices. This script seamlessly facilitates the estimation of all $\alpha$ values by solving these equations concurrently.

Our Python code numerically estimates the $\alpha$ values based on the sampled data. 

## How to Use the Script

### Prerequisites

Ensure you have `Python` installed along with the following libraries:
- `pandas`
- `scipy`
- `tabulate`

You can install them using pip:
```sh
pip install pandas scipy tabulate
```

### Script Usage

The script can be used via the command line and accepts various arguments:

- `--input`: Path to the input file (Default: 'input.csv')
- `--output`: Path to the output file (Default: 'output.txt')
- `--phivalue`: Set phi value, values should be between 0 and 1 (Default: 1)

#### Example Usage:

```sh
python HCV_alpha_matrix_calculation.py --input path_to_input_file --output path_to_output_file --phivalue 0.8
```

### Input File Format

Ensure your input file is formatted correctly as a tab-delimited CSV file with the following columns:
- `h1`: Frequencies of the Host type 1.
- `p1`: Frequencies of the parasite type 1.
- `CSA`: Estimate of the CSA index.
- `HS`: Estimate of the HS index.
- `PI`: Estimate of the PI index.
- `HP`: Estimate of the HP index.

### Output

The script will output a file specified by the `--output` argument (or 'output.txt' by default) that contains the original data along with the estimated $\alpha$ values.

