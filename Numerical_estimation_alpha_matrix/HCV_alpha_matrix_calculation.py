#!/usr/bin/env python
# coding: utf-8

import argparse
import pandas as pd
from scipy.optimize import least_squares
from tabulate import tabulate
import os

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, required=False, default='input.csv', help='Path to input file')
    parser.add_argument('--output', type=str, required=False, default='output.txt', help='Path to output file')
    parser.add_argument('--phivalue', type=float, required=False, default=1, help='Set phi value [values between 0 - 1]')
    return parser.parse_args()

def load_data(inputfile, phi):
    df = pd.read_csv(inputfile, delimiter='\t')
    df['phi'] = phi
    return df

def calculate_alpha_values(row):
    h1, p1, CSA, HS, PI, HP, phi = row[['h1', 'p1', 'CSA', 'HS', 'PI', 'HP', 'phi']]
    csa2, p2, h2 = CSA**2, 1-p1, 1-h1
    
    def equations(z):
        a, b, c, d = z
        return [
            phi*((a-c)*p1+(b-d)*p2)-HS,
            (phi**2)*((p2**2)*b*d-(p1**2)*a*c)-PI,
            phi*(b*p2*(1-phi*d*p2)-c*p1*(1-phi*a*p1))-HP,
            (h1*h2*p1*p2*(a*d - b*c)**2) - csa2*((a*p1 + b*p2)*(c*p1 + d*p2)*(a*h1 + c*h2)*(b*h1 + d*h2))
        ]
    
    res = least_squares(equations, [1,1,1,1], bounds=(0, 1)).x
    return pd.Series(res, index=["a11","a12","a21","a22"])

def main():
    args = parse_arguments()
    inputfile, outfile, phi = args.input, args.output, args.phivalue
    
    if not os.path.exists(inputfile):
        raise FileNotFoundError(f"Input file {inputfile} not found.")
    
    df = load_data(inputfile, phi)
    print("Input data:")
    print(tabulate(df.head(), headers='keys', tablefmt='psql'))
    
    alpha_values = df.apply(calculate_alpha_values, axis=1)
    df_result = pd.concat([df, alpha_values], axis=1)
    
    print("Output data:")
    print(tabulate(df_result.head(), headers='keys', tablefmt='psql'))
    
    df_result.to_csv(outfile, sep='\t', index=False, float_format='%1.10f')

if __name__ == "__main__":
    main()

