#$-cwd
#$-l vf=5G
#$-pe serial 2
#$-q bigmem
#$-N simul_alpha_matr
#$-t 1-14

#-t should be the number of lines in the simulation_schedule.txt
echo ${SGE_TASK_ID}

# Read parameters from simulation_schedule.txt
read -a arr < <(sed -n ${SGE_TASK_ID}p simulation_schedule.txt)
mat=${arr[0]}
alpha_threshold=${arr[1]}
phi=${arr[2]}
fsuffix=${arr[3]}

# Echo the parameters for logging
echo "Matrix: ${mat}"
echo "Alpha Threshold: ${alpha_threshold}"
echo "Phi: ${phi}"
echo "File Suffix: ${fsuffix}"


# Run the Python script with the parameters
python ./Generate_random_matrices_v5.py --matrix ${mat} \
                                        --seed $((100*$SGE_TASK_ID)) \
                                        --fsuffix ${fsuffix} \
                                        --phi ${phi} \
                                        --alpha_threshold ${alpha_threshold} \