import sys
import argparse
import random
sys.path.append("./")
import FunGenMatr



# Set up the command line parser
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--matrix', type=str,help='matrix to simulate. Default ',required=False,default='MA',choices={'MA', 'GFG', 'iGFG', 'HR', 'PI', 'neutral'})
parser.add_argument('--output',type=str,required=False, default='.',help='Path to output file')
parser.add_argument('--seed',type=int,required=False, default=-999,help='Manually set seed for simulation. Note if -999, random number generator will not be initialized with seed.')
parser.add_argument('--nsamples',type=int,required=False, default=5000,help='Number of samples to simulate')
parser.add_argument('--alpha_threshold', type=float, required=False, default=0.1, help='Threshold for varying alpha')    
parser.add_argument(
    '--h1_prior', 
    nargs=2,
    type=FunGenMatr.ranged_type(float, 0.0, 1.0), 
    metavar=('h1_lower', 'h1_upper'),
    default=[0.05, 0.5], 
    help='Lower and upper value for the uniform prior of h1.',
    )
parser.add_argument('--p1_prior',
    nargs=2, 
    type=FunGenMatr.ranged_type(float, 0.0, 1.0), 
    metavar=('p1_lower', 'p1_upper'), 
    default=[0.05, 0.5], 
    help='Lower and upper value for the uniform prior for p1.'
    )
parser.add_argument('--n_H', type=int,  default=1006, help='Size of the haploid healthy sample')
parser.add_argument('--n_I', type=int, default=902,  help='Size of the haploid infected sample')
parser.add_argument('--phi', type=FunGenMatr.ranged_type(float, 0.0, 1.0),  default=0.05, help='Disease prevalence' )
parser.add_argument('--Pop_size',  type=int,  default= 100000, help='Total population size')
parser.add_argument('--fsuffix',  type=str,  default= '', help='Suffix to add to output files')
parser.add_argument('--maxIt',  type=int,  default= 6000, help='Maximum number of attempts to create the nsamples')


#####################################################################
# MAIN ##############################################################
#####################################################################

# Parse the arguments from the command line
args = parser.parse_args()
#parser.print_help()

# Extract the parsed arguments
mat = args.matrix
print('matrix is:'+ mat)
nsamples = args.nsamples
print('Simulating n=' + str(nsamples) + ' samples')
out = args.output

if args.seed != (-999):
	random.seed(args.seed)

fsuffix = args.fsuffix


h1_lower = args.h1_prior[0]
h1_upper = args.h1_prior[1]

p1_lower = args.p1_prior[0]
p1_upper = args.p1_prior[1]

phi = args.phi
Pop_size = args.Pop_size
n_I = args.n_I
n_H = args.n_H
alpha_threshold = args.alpha_threshold
out_prefix =out + '/' + mat + '_' + str(nsamples) + '_a_' + str(alpha_threshold) +'_phi_'+ str(phi) + '_run_' + fsuffix 
outfile_pop= out_prefix + '_pop.txt'
outfile_sample= out_prefix + '_sample.txt'
maxIt = args.maxIt

if maxIt < nsamples:
    raise ValueError('Number of samples to create smaller than the nunber of attempts.')

# Prepare the output files
outf_pop = open(outfile_pop, "w")
outf_sample = open(outfile_sample, "w")
# Write the final argument settings to output file as a comment
for key in vars(args):
    print('#' + key + '=' + str(getattr(args, key)),file=outf_pop)
    print('#' + key + '=' + str(getattr(args, key)),file=outf_sample)
header=['i', 'h1', 'p1', 'alpha_11', 'alpha_12', 'alpha_21', 'alpha_22', 
'HS', 'PI', 'HP', 'CSA', 'F_1z', 'F_2z', 'F_11', 'F_12', 'F_21', 'F_22']

print('\t'.join(map(str,header)), file=outf_pop)
print('\t'.join(map(str,header)), file=outf_sample)
 
i = 0
totalIt = 0
popfail = 0
samplefail = 0

 
while ((i < nsamples) and (totalIt < maxIt)):
    print('Running total iteration ' + str(totalIt + 1))
    alpha_mat = FunGenMatr.generate_alpha(alpha_threshold,mat=mat)
    h1_freq = FunGenMatr.draw_freq(h1_lower, h1_upper, 'uniform')
    p1_freq = FunGenMatr.draw_freq(p1_lower, p1_upper, 'uniform')

    print('Construction of population')
    # Construct the population
    pop = FunGenMatr.construct_pop(phi=phi, h1=h1_freq, p1=p1_freq,  alpha=alpha_mat, Pop_size=Pop_size)
    
    # Check if all alleles are present in the infected subsample
    popInf_check = FunGenMatr.infcheck(pop)
    if popInf_check < 3:
        print('Generation of population failed')
        popfail +=1
        totalIt +=1
        continue
    
    print('Population generation successful')
    # Use if construct_pop returns a list
    #indices_pop=calculate_indices_abs(*pop)
    # Use if construct_pop returns a dict
    indices_pop = FunGenMatr.calculate_indices_abs(**pop)
    #print(indices_pop)

    f_1z_tilde = pop['F_1z']/pop['Healthy_size']
    f_2z_tilde = pop['F_2z']/pop['Healthy_size']
    f_11_tilde = pop['F_11']/pop['Inf_size']
    f_12_tilde = pop['F_12']/pop['Inf_size']
    f_21_tilde = pop['F_21']/pop['Inf_size']
    f_22_tilde = pop['F_22']/pop['Inf_size']

    print('Generating sample')
    sample = FunGenMatr.generate_sample(f_1z_tilde=f_1z_tilde, f_2z_tilde=f_2z_tilde, f_11_tilde=f_11_tilde, f_12_tilde=f_12_tilde ,f_21_tilde=f_21_tilde, f_22_tilde=f_22_tilde, n_H=n_H, n_I=n_I)
        
    sampleInf_check = FunGenMatr.infcheck(sample)
    if sampleInf_check < 3:
        print(' Sample generation failed')
        samplefail +=1
        totalIt +=1
        continue

    print('Sample generation successful')
    # Calculate the indices for the sample by during the dict into keyword parameters
    indices_sample = FunGenMatr.calculate_indices_abs(**sample)

    
    
    # Write to output file
    baselist=[i, round(h1_freq, 5), round(p1_freq, 5), round(alpha_mat['alpha_11'], 5), round(alpha_mat['alpha_12'], 5), 
    round(alpha_mat['alpha_21'], 5), round(alpha_mat['alpha_22'], 5)] 
    
    outlist_pop=[*baselist, round(indices_pop['HS'], 5), round(indices_pop['PI'], 5), round(indices_pop['HP'], 5), 
    round(indices_pop['CSA'], 5),pop['F_1z'], pop['F_2z'], pop['F_11'],
    pop['F_12'],pop['F_21'], pop['F_22']]
    
    print('\t'.join(map(str,outlist_pop)), file=outf_pop)
    
    outlist_sample=[*baselist,
    round(indices_sample['HS'], 5), 
    round(indices_sample['PI'], 5), 
    round(indices_sample['HP'], 5), 
    round(indices_sample['CSA'], 5), 
    sample['F_1z'], 
    sample['F_2z'], 
    sample['F_11'],
    sample['F_12'],
    sample['F_21'], 
    sample['F_22']]
    
    print('\t'.join(map(str,outlist_sample)), file=outf_sample)
    #print('test', file=outf)
    i += 1
    totalIt +=1

print('Succesfully generated ' + str(i) + ' samples out of ' + str(totalIt) + ' attempts')

print('Number of attempts where population generation failed: '+ str(popfail))  
print('Number of attemps where pop could be generated but sample was unsuccessful: '+ str(samplefail))     

outf_pop.close()
outf_sample.close()
