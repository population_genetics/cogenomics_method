# 4.) Generating Alpha Matrix Simulations for the ABC Model Choice

## Table of Contents
- [Overview](#overview)
- [Simulation Workflow](#Simulation-Workflow)
- [Detailed Simulation Steps](#Detailed-Simulation-Steps)
- [Detailed Usage Guide](#Detailed-Usage-Guide)
- [Usage Example](#Usage-Example)
- [Tutorial](#Tutorial)
  - [Setting Up the Simulation Schedule](#Setting-Up-the-Simulation-Schedule)
  - [Running the Simulations](#Running-the-Simulations)
  - [Understanding the Output Files](#Understanding-the-Output-Files)

## Overview

This directory contains the necessary scripts and data to perform simulations for ABC model choice analysis across various matrices. The primary objective is to generate datasets for different matrices, adhering to specific simulation protocols. Each simulation extracts parameter values from corresponding distributions and calculates indices for a population, including a random sample of both infected and uninfected individuals.

### Simulation Workflow

The simulation process involves drawing random parameter values from biologically relevant ranges, generating matrix configurations, calculating abundances and frequencies, and running checks on the samples generated. A comprehensive guide to each simulation step is available in the ["Detailed Simulation Steps"](#Detailed-Simulation-Steps) section.

## Detailed Simulation Steps
<a name="#Detailed-Simulation-Steps"></a>
The simulation is implemented as follows:

1. Draw a random value for $h_1$ from $\mathcal{U}(0.05,0.5)$.
2. Draw a random value for $p_1$ from $\mathcal{U}(0.05,0.5)$.
3. Randomly draw one valid matrix configuration for the matrix of interest.
4. Randomly draw the coefficients of the matrix from the corresponding uniform distributions.
5. Calculate all abundances and frequencies for a population of size $N_H=100,000$ hosts and a given value of $\phi$.
6. Calculate the value of each index using data from the entire population.
7. Draw a sample from the **non-infected** part of the population.
8. Draw a sample from the **infected part** of the population.
9. Check if all host and pathogen alleles are present in the infected sample $n_I$. If not, discard the simulation.
10. Calculate the indices for the sample.

This procedure is repeated until $m=50,000$ simulations are generated for a given matrix or if a maximum number of $m_I$ attempts to generate simulations is reached.

## Detailed Usage Guide
<a name="Detailed-Usage-Guide"></a>

### 1. R-script: `Generate_simulation_schedule.R`
**Purpose**: Generates `simulation_schedule.txt`, containing matrix type, delta value, phi value, and task ID for the simulation schedule.

### 2. Output file: `simulation_schedule.txt`
**Contents**: This file, created by `Generate_simulation_schedule.R`, lists parameters for each simulation run. It includes matrix type, alpha threshold, phi, and runID (default 5000 simulations per run).

### 3. Python script: `Generate_random_matrices_v5.py`
**Functionality**: Produces random matrices based on input parameters. It utilizes functions from `FunGenMatr.py` and accepts arguments like matrix type, alpha threshold, etc., via command line.

#### Command-line Options

- **`--matrix`**: Specifies the matrix for simulation. Options include `MA`, `GFG`, `iGFG`, `HR`, `PI`, `neutral`. Default: `MA`.
- **`--output`**: Path to output file. Default: current directory.
- **`--seed`**: Seed for simulation. If `-999`, a random seed is used. Default: `-999`.
- **`--nsamples`**: Number of samples to simulate. Default: `5000`.
- **`--alpha_threshold`**: Threshold for varying alpha. Default: `0.1`.
- **`--h1_prior`** & **`--p1_prior`**: Lower and upper value for the uniform prior of h1 and p1, respectively. Default: [`0.05`, `0.5`].
- **`--n_H`** & **`--n_I`**: Size of the haploid healthy and infected sample, respectively. Defaults: `1006` for `n_H`, `902` for `n_I`.
- **`--phi`**: Disease prevalence. Default: `0.05`.
- **`--Pop_size`**: Total population size. Default: `100000`.
- **`--fsuffix`**: Suffix to add to output files.
- **`--maxIt`**: Maximum number of attempts to create the nsamples. Default: `6000`.

### 4. Python Script: `FunGenMatr.py`
Defines various functions utilized in matrix generation, including functions to validate inputs, generate alpha, construct populations, draw frequencies, calculate indices, and others.

### 5. Bash Script: `run_simulations_co_GWAs_fsched_SGE.sh` or `run_simulations_co_GWAs_fsched_SLURM.sh`
**Simulation Execution**: This Bash script is used for scheduling the execution of `Generate_random_matrices_v5.py` on a computing cluster using SGE or SLURM job scheduler. It extracts parameters from `simulation_schedule.txt` and executes `Generate_random_matrices_v5.py` with these parameters.

## Usage Example:
1.) To generate a simulation schedule, simply execute the script using R. Ensure the `tidyverse` library is installed.

```sh
Rscript Generate_simulation_schedule.R
```
The produced `simulation_schedule.txt` will be utilized by `run_simulations_co_GWAs_fsched_*.sh` to execute simulations in parallel.

2a.) Submit `run_simulations_co_GWAs_fsched_SGE.sh` to SGE:

```sh
qsub run_simulations_co_GWAs_fsched_SGE.sh
```

2b.) Submit `run_simulations_co_GWAs_fsched_SLURM.sh` to SGE:

```sh
sbatch run_simulations_co_GWAs_fsched_SLURM.sh
```

## Tutorial

### Set Simulation Schedule
To prepare for each matrix's simulations, first execute [Generate_simulation_schedule.R](./Generate_simulation_schedule.R). This R-script configures the simulation schedule.

```R
# Rscript to generate a simulation schedule

require("tidyverse")

# matrices to simulate
matrices <- c("neutral","GFG","iGFG","MA","PI","HR") 
# alpha_thresholds to simulate
alpha_threshold <- seq(0.1,0.3,by=0.1)
# phi values to simulate
phi <- c(0.05,0.5,0.95)
# number of tasks to simulate
tasks <- 1:10

# Generate all pairwise combinations
sim_schedule <- expand.grid(mat=matrices,alpha_threshold=alpha_threshold,phi=phi,taskID=tasks)

write_delim(sim_schedule,"simulation_schedule_tutorial.txt",col_names=F)
```

This script generates a simulation schedule by combining different matrices (`matrices`), alpha threshold values (`alpha_threshold`), phi values (`phi`), and task IDs (`tasks`). The output is written to `simulation_schedule_tutorial.txt`, which serves as a roadmap for the simulations.

**Example of Simulation Schedule (`simulation_schedule_tutorial.txt`):**

```text
neutral 0.1 0.05 1
GFG 0.1 0.05 1
iGFG 0.1 0.05 1
MA 0.1 0.05 1
PI 0.1 0.05 1
HR 0.1 0.05 1
neutral 0.2 0.05 1
GFG 0.2 0.05 1
iGFG 0.2 0.05 1
MA 0.2 0.05 1
PI 0.2 0.05 1
HR 0.2 0.05 1
neutral 0.3 0.05 1
GFG 0.3 0.05 1
iGFG 0.3 0.05 1
MA 0.3 0.05 1
PI 0.3 0.05 1
HR 0.3 0.05 1
neutral 0.1 0.5 1
GFG 0.1 0.5 1
iGFG 0.1 0.5 1
MA 0.1 0.5 1
PI 0.1 0.5 1
HR 0.1 0.5 1
neutral 0.2 0.5 1
....
```
Each line represents a unique combination of matrix, alpha threshold, phi value, and task ID. For example, 10 replicates (task IDs) for each combination with 100 samples per replicate (`--nsamples` in `Generate_random_matrices_v5.py`) would yield 1000 simulations.

### Running the Simulations

To conduct simulations, the following files are essential:

1) `simulation_schedule_tutorial.txt`: This file outlines the schedule for simulations.
2) `Generate_random_matrices_v5.py`: Performs the actual simulations, using `FunGenMatr.py` for necessary functions.
3) `FunGenMatr.py`:  Contains core functions for `Generate_random_matrices_v5.py`.
4) `run_simulations_co_GWAs_fsched_SLURM_tutorial.sh`: Bash script to execute simulations on a cluster.

**Example from `run_simulations_co_GWAs_fsched_SLURM_tutorial.sh`:**

```bash
#!/bin/bash
#SBATCH --job-name=simul_alpha_matr_tutorial
#SBATCH --output=%x_%A_%a.out
#SBATCH --partition=CPU-all
#SBATCH --mem=5G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --array=1-540
#SBATCH --nodelist=node13

#--array should be the number of lines in the simulation_schedule.txt
echo ${SLURM_ARRAY_TASK_ID}

# Read parameters from simulation_schedule_tutorial.txt
read -a arr < <(sed -n ${SLURM_ARRAY_TASK_ID}p simulation_schedule_tutorial.txt)
mat=${arr[0]}
alpha_threshold=${arr[1]}
phi=${arr[2]}
fsuffix=${arr[3]}



# Echo the parameters for logging
echo "Matrix: ${mat}"
echo "Alpha Threshold: ${alpha_threshold}"
echo "Phi: ${phi}"
echo "File Suffix: ${fsuffix}"

# Run the Python script with the parameters
python ./Generate_random_matrices_v5.py --matrix ${mat} \
                                        --seed $((100*$SLURM_ARRAY_TASK_ID)) \
                                        --fsuffix ${fsuffix} \
                                        --phi ${phi} \
                                        --alpha_threshold ${alpha_threshold} \
                                        --nsamples 100 \
                                        --n_I 100 \
                                        --n_H 104 \

```

The `run_simulations_co_GWAs_fsched_SLURM_tutorial.sh` script utilizes simulation_schedule_tutorial.txt as its primary input, facilitating customization based on specific dataset characteristics and disease prevalence. For a comprehensive list of adjustable options, please refer to the Detailed Usage Guide. In our example, the sample sizes have been predetermined: `nH` (the number of healthy haploid hosts) is set to `104`, and `nI` (the number of infected haploid hosts) is set to `100`. These values correspond to `52` healthy diploid hosts and `50` infected diploid hosts. Although these sample sizes are relatively small and primarily chosen for illustrative purposes, they may limit the biological implications of the results. Additionally, the `--nsamples` parameter is fixed at `100`, leading to a total of `1000` simulations, given that we have established `10` replicates for each parameter combination in the simulation schedule.

### Understanding the Output Files

The simulation generates two primary types of files for each matrix combination: a population file (`*_pop.txt`) and a sample file (`*_sample.txt`). Here's a snippet of what these files contain:

**Example of Population File Header (`*_pop.txt`):**

|#matrix=GFG|
|----------|
|#output=.|
|#seed=2000|
|#nsamples=100|
|#alpha_threshold=0.1|
|#h1_prior=[0.05, 0.5] |
|#p1_prior=[0.05, 0.5] |
|#n_H=104|
|#n_I=100|
|#phi=0.5|
|#Pop_size=100000|
|#fsuffix=1|
|#maxIt=6000|

|i                                                                                                 |h1   |p1     |alpha_11|alpha_12|alpha_21|alpha_22|HS     |PI     |HP     |CSA    |F_1z|F_2z|F_11|F_12|F_21|F_22|
|--------------------------------------------------------------------------------------------------|-----|-------|--------|--------|--------|--------|-------|-------|-------|-------|----|----|----|----|----|----|
|0                                                                                                 |0.39319|0.43344|0.90496 |0.97685 |0.99476 |0.08347 |0.23362|0.03574|0.09688|0.49934|20727.137231615998|46164.40273829653|7711.302787691868|10880.530921956703|13081.736163467252|1434.8901569716381|
|1                                                                                                 |0.41893|0.11572|0.95746 |0.0139  |0.97554 |0.91879 |0.40114|0.00063|0.04967|0.56041|39314.774967291974|31222.166261037706|2320.785100447334|257.42149363902877|3279.7741212138844|23605.07805637007|
|2                                                                                                 |0.40792|0.43266|0.93438 |0.94235 |0.09654 |0.92064 |0.18741|0.06559|0.18084|0.40322|21642.039435383314|42508.83342949405|8245.39830505171|10904.413957857285|1236.569143300096|15462.745728913544|
|3                                                                                                 |0.4736|0.49016|0.91938 |0.90673 |0.90333 |0.02253 |0.22933|0.04856|0.05831|0.48806|25741.56542378032|40683.98169851584|10671.198211875793|10946.763497857404|11654.093640230665|302.3975277399661|
|4                                                                                                 |0.25401|0.07375|0.05825 |0.99849 |0.92192 |0.91226 |0.00808|0.1953 |0.23313|0.13226|13600.372938879895|40545.607275419155|54.55999520937305|11745.892982338906|2536.18560230355|31517.381205849128|
|5                                                                                                 |0.17738|0.33919|0.91968 |0.04681 |0.98923 |0.98213 |0.32083|0.02115|0.13115|0.29871|14697.400931111359|41766.5923079707|2766.699128864369|274.3616939359188|13800.794308473285|26694.15162964437|
|6                                                                                                 |0.1062|0.23649|0.97902 |0.96011 |0.09927 |0.90398 |0.12545|0.12513|0.22966|0.29757|5498.049185555122|57486.06952026127|1229.4143391266894|3892.481689711781|1049.174642234552|30844.810623110592|
|7                                                                                                 |0.11635|0.11028|0.91964 |0.00119 |0.99313 |0.91236 |0.40939|0.00256|0.05167|0.30738|11039.105940848827|47661.0275478762|590.0121564453973|6.163972413649646|4838.956344739784|35864.73403767615|
|8                                                                                                 |0.27636|0.2909 |0.95521 |0.06192 |0.98965 |0.95395 |0.32128|0.01257|0.10942|0.37217|23190.063554881643|37472.144528918005|3839.606401517201|606.7018801811657|10416.265220978275|24475.218413523704|
|9                                                                                                 |0.19894|0.43155|0.9788  |0.02206 |0.90064 |0.93185 |0.24172|0.03938|0.14868|0.33654|15567.716870407105|43321.941403748024|4201.701196255224|124.71687436283385|15567.574411819218|21216.34924340759|
|10                                                                                                |0.41286|0.23023|0.93885 |0.04606 |0.99911 |0.98878 |0.36978|0.00568|0.0916 |0.47915|36092.25468699842|29616.519823706112|4461.9473247633405|731.8392845820515|6752.729132891899|22344.709747058176|


This file includes the results for the entire simulated population for a given matrix, alpha threshold, phi value, and task ID.

**Example of Sample File Header (`*_sample.txt`):**

|#matrix=GFG|
|----------|
|#output=.|
|#seed=2000|
|#nsamples=100|
|#alpha_threshold=0.1|
|#h1_prior=[0.05, 0.5] |
|#p1_prior=[0.05, 0.5] |
|#n_H=104|
|#n_I=100|
|#phi=0.5|
|#Pop_size=100000|
|#fsuffix=1|
|#maxIt=6000|

|i                                                                                                 |h1   |p1     |alpha_11|alpha_12|alpha_21|alpha_22|HS     |PI     |HP     |CSA    |F_1z|F_2z|F_11|F_12|F_21|F_22|
|--------------------------------------------------------------------------------------------------|-----|-------|--------|--------|--------|--------|-------|-------|-------|-------|----|----|----|----|----|----|
|0                                                                                                 |0.39319|0.43344|0.90496 |0.97685 |0.99476 |0.08347 |0.2352 |0.10406|0.06112|0.54032|36  |68  |27  |31  |41  |1   |
|1                                                                                                 |0.41893|0.11572|0.95746 |0.0139  |0.97554 |0.91879 |0.54203|0.00057|0.08189|0.45072|55  |49  |6   |1   |14  |79  |
|2                                                                                                 |0.40792|0.43266|0.93438 |0.94235 |0.09654 |0.92064 |0.14647|0.1115 |0.16429|0.40876|40  |64  |24  |29  |4   |43  |
|3                                                                                                 |0.4736|0.49016|0.91938 |0.90673 |0.90333 |0.02253 |0.23489|0.14247|0.00019|0.44624|38  |66  |37  |23  |40  |0   |
|4                                                                                                 |0.25401|0.07375|0.05825 |0.99849 |0.92192 |0.91226 |0.12493|0.24629|0.29362|0.03617|19  |85  |1   |26  |4   |69  |
|5                                                                                                 |0.17738|0.33919|0.91968 |0.04681 |0.98923 |0.98213 |0.34369|0.01585|0.10241|0.36024|30  |74  |7   |1   |24  |68  |
|6                                                                                                 |0.1062|0.23649|0.97902 |0.96011 |0.09927 |0.90398 |0.01087|0.21196|0.22174|0.10206|10  |94  |1   |9   |3   |87  |
|7                                                                                                 |0.11635|0.11028|0.91964 |0.00119 |0.99313 |0.91236 |0.31981|0.01339|0.04911|0.55825|22  |82  |6   |0   |11  |83  |
|8                                                                                                 |0.27636|0.2909 |0.95521 |0.06192 |0.98965 |0.95395 |0.37656|0.01975|0.11468|0.40407|39  |65  |9   |1   |24  |66  |
|9                                                                                                 |0.19894|0.43155|0.9788  |0.02206 |0.90064 |0.93185 |0.3254 |0.05159|0.18056|0.31314|28  |76  |8   |0   |39  |53  |
|10                                                                                                |0.41286|0.23023|0.93885 |0.04606 |0.99911 |0.98878 |0.39829|0.02132|0.12175|0.4565 |54  |50  |14  |2   |23  |61  |


This file contains data for a sample drawn from the simulated population. It's structured similarly to the population file but focuses on the sampled subset.

Both file types provide detailed data on simulated frequencies for host and pathogen samples and overall populations, along with the the indices for each parameter combination.