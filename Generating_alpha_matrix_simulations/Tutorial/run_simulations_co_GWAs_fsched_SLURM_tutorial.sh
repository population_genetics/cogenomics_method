#!/bin/bash
#SBATCH --job-name=simul_alpha_matr_tutorial
#SBATCH --output=%x_%A_%a.out
#SBATCH --partition=CPU-all
#SBATCH --mem=5G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --array=1-540
#SBATCH --nodelist=node13

#--array should be the number of lines in the simulation_schedule.txt
echo ${SLURM_ARRAY_TASK_ID}

# Read parameters from simulation_schedule.txt
read -a arr < <(sed -n ${SLURM_ARRAY_TASK_ID}p simulation_schedule_tutorial.txt)
mat=${arr[0]}
alpha_threshold=${arr[1]}
phi=${arr[2]}
fsuffix=${arr[3]}



# Echo the parameters for logging
echo "Matrix: ${mat}"
echo "Alpha Threshold: ${alpha_threshold}"
echo "Phi: ${phi}"
echo "File Suffix: ${fsuffix}"

# Run the Python script with the parameters
python ./Generate_random_matrices_v5.py --matrix ${mat} \
                                        --seed $((100*$SLURM_ARRAY_TASK_ID)) \
                                        --fsuffix ${fsuffix} \
                                        --phi ${phi} \
                                        --alpha_threshold ${alpha_threshold} \
                                        --nsamples 100 \
                                        --n_I 100 \
                                        --n_H 104 \
                                        
                                        
