import math
import pandas as pd
import random
import numpy as np
import sys
import argparse




# Function to define acceptable ranges for command line arguments
# Solution from https://stackoverflow.com/questions/55324449/how-to-specify-a-minimum-or-maximum-float-value-with-argparse
# With little adjustment to the way how the error is risen
def ranged_type(value_type, min_value, max_value):
    """
    Return function handle of an argument type function for ArgumentParser checking a range:
        min_value <= arg <= max_value

    Parameters
    ----------
    value_type  - value-type to convert arg to
    min_value   - minimum acceptable argument
    max_value   - maximum acceptable argument

    Returns
    -------
    function handle of an argument type function for ArgumentParser


    Usage
    -----
        ranged_type(float, 0.0, 1.0)

    """

    def range_checker(arg: str):
        try:
            f = value_type(arg)
        except ValueError:
            raise argparse.ArgumentTypeError(f'must be a valid {value_type}')
        if f < min_value or f > max_value:
            raise argparse.ArgumentTypeError(f'must be within [{min_value}, {max_value}]')
        return f

    # Return function handle to checking function
    return range_checker


# Function to generate the matrix
def generate_alpha(random_alpha_threshold,mat='MA'):
    pos_mats = ['GFG','MA','iGFG','HR','PI','neutral']
    if not mat in pos_mats:
        print('Matrix not in list of defined matrices')
        return
    else:
        # Dictionary for return values
        alpha_return = {}
        # List of alpha names
        alpha_list = ['alpha_11', 'alpha_12', 'alpha_21', 'alpha_22']
  
        # General outline how matrices are generated:
        # I. Construct a dictionary int_dict for an  intermediate matrix consisting of only 0s and 1s 
        # II. Draw all alphas from appropriate uniform distributions based on this matrix
        # The detailed construction of the matrix works as follows:
        # ind is an indicator variable indicating if the matrix is an iGFG matrix or not
        # if matrix is a iGFG matrix, then the default for all alphas is 0 (constructed as 1-ind)
        # for all other matrices the initial default value is 1-ind=1
        # The further construction of the matrices proceeds as follows:
        # 1. The first alpha to have a distinct value is chosen.
        # This alpha is assigned a value of ind (ind=0 for GFG, MA, PI, HR; ind=1 for iGFG)
        # 2. The second alpha to have a distinct value is chosen for MA, PI and HR, based on the index of alpha chosen before in the alpha_list
        # 3. Then actual values of all alphas are chosen from the appropriate uniform distribution
        # For all alphas so far being 0 the alpha value should be chosen from a unif(0, alpha_threshold)
        # For all alphas so far being 1 the alpha value should be chosen from a unif(1-alpha_threshold,1)
        # This is achived using the following expression:
        # random.uniform(ind_dict[alpha]*(1-random_alpha_threshold),1*ind_dict[alpha]+(1-ind_dict[alpha])*(random_alpha_threshold))
        
        ind = (mat=='iGFG')
        # Create a dictionary of alphas, where each alpha is initially associated with the default value
        int_dict=dict.fromkeys(alpha_list,1-ind)
        selected=random.choice((alpha_list))
        # Get the the index of the selected alpha in the alpha list
        a = alpha_list.index(selected)
        if mat != 'neutral':
        	# The first selected alpha gets the non-default value
        	int_dict[selected] = 1*ind
        # Select the second alpha which should get a 'non-default' value if necessary
        # (we are already done for GFG and iGFG)
        # pic is the name of the second alpha to get a draw from the opposite distribution
        if mat == 'MA':
            pic=alpha_list[a*(-1)+3]
            int_dict[pic] = 1*ind            
        elif mat == 'PI':
            pic=alpha_list[a + pow(-1, (a>1))*2]
            int_dict[pic] = 1*ind           
        elif mat == 'HR':
            pic=alpha_list[a + pow(-1, a%2)]
            int_dict[pic] = 1*ind
        else:
            pic=''
        # Draw each alpha from a uniform distribution
        for alpha in int_dict:
            alpha_return[alpha]=random.uniform(int_dict[alpha]*(1-random_alpha_threshold),1*int_dict[alpha]+(1-int_dict[alpha])*(random_alpha_threshold))
            
        return alpha_return

def construct_pop(phi: float, h1: float, p1: float, alpha:dict, Pop_size: int):
    alpha_11 = alpha['alpha_11']
    alpha_12 = alpha['alpha_12']
    alpha_21 = alpha['alpha_21']
    alpha_22 = alpha['alpha_22']
    
    pop_dict = dict.fromkeys(['F_11', 'F_12', 'F_21', 'F_22', 'F_1z', 'F_2z', 'Inf_size' , 'Healthy_size'], 0)
    # Calculate the total number of individuals exposed to pathogens
    Exp_Pop = phi*Pop_size
    # Calculate the total number of individuals not exposed to pathogens
    Heal_Pop = (1-phi)*Pop_size
    
    h2 = (1-h1)
    p2=(1-p1)

    #Subtypes
    # Calculate the total number of individuals in the host population which have genotype 1 and are infected by pathogens with genotype 1 
    F_11 = alpha_11 * h1 * p1 * Exp_Pop
    pop_dict['F_11'] = F_11
    # Calculate the total number of individuals in the host population which have genotype 1 and are infected by pathogens with genotype 2 
    F_12 = alpha_12 * h1 * p2 * Exp_Pop
    pop_dict['F_12'] = F_12
    # Calculate the total number of individuals in the host population which have genotype 2 and are infected by pathogens with genotype 1 
    F_21 = alpha_21 * h2 * p1 * Exp_Pop
    pop_dict['F_21'] = F_21
    # Calculate the total number of individuals in the host population which have genotype 2 and are infected by pathogens with genotype 2 
    F_22 = alpha_22 * h2 * p2 * Exp_Pop
    pop_dict['F_22'] = F_22
    # Calculate the total number of individuals in the host population which have genotype 1 and are not infected
    F_1z = h1 * Heal_Pop + (1-alpha_11) * h1 * p1 * Exp_Pop + (1-alpha_12) * h1 * p2 * Exp_Pop
    pop_dict['F_1z'] = F_1z
    # Calculate the total number of individuals in the host population which have genotype 2 and are not infected
    F_2z = h2 * Heal_Pop + (1-alpha_21) * h2 * p1 * Exp_Pop + (1-alpha_22) * h2 * p2 * Exp_Pop
    pop_dict['F_2z'] = F_2z

    # Calculate the total number of infected individuals
    Inf_size = F_11 + F_12 + F_21 + F_22
    pop_dict['Inf_size'] = Inf_size
    # Calculate the total number of non-infected individuals
    Healthy_size = F_1z + F_2z
    pop_dict['Healthy_size'] = Healthy_size
    #return(F_11, F_12 , F_21 , F_22,  F_1z,  F_2z,  Inf_size, Healthy_size)
    return(pop_dict)
    

# Function for drawing frequencies of a given genotype from a given distribution
def draw_freq(lower_threshold, upper_threshold, distribution='uniform'):
    if distribution not in ['uniform']:
        sys.exit('Unknown distribution')
    if(lower_threshold > upper_threshold):
        sys.exit('Lower threshold larger than upper threshold')
    freq = random.uniform(lower_threshold, upper_threshold)
    return(freq)
    
def calculate_indices(f_11, f_12, f_21, f_22, f_1z, f_2z, f_11_tilde, f_12_tilde, f_21_tilde, f_22_tilde):
    f2_bar= (f_11+f_12+f_1z) * (f_21+f_22+f_2z)   
    f1_bar = math.sqrt((f_11_tilde + f_12_tilde)*(f_21_tilde + f_22_tilde)*(f_11_tilde + f_21_tilde)*(f_12_tilde + f_22_tilde))    
    indices_dict={}
    #Calculate index
    if f2_bar>0:
        indices_dict['HS']=abs((f_11+f_12)*f_2z -(f_21+f_22)*f_1z)/f2_bar
        indices_dict['PI']=abs((f_12*f_22) - (f_21*f_11))/f2_bar
        indices_dict['HP']=abs((f_12*f_2z)-(f_21*f_1z))/f2_bar
    else:
        indices_dict['HS']=''
        indices_dict['PI']=''
        indices_dict['HP']=''

    if f1_bar>0:
        indices_dict['CSA']=abs((f_11_tilde*f_22_tilde) -(f_12_tilde*f_21_tilde))/f1_bar
    else:
        indices_dict['CSA']= ''
    return(indices_dict)
    
def calculate_indices_abs(F_11, F_12, F_21, F_22, F_1z, F_2z, Inf_size, Healthy_size):
    Tot_size=Inf_size + Healthy_size
    f_11 = F_11/Tot_size
    f_12 = F_12/Tot_size
    f_21 = F_21/Tot_size
    f_22 = F_22/Tot_size
    f_1z=  F_1z/Tot_size
    f_2z= F_2z/Tot_size
    f_11_tilde = F_11/Inf_size
    f_12_tilde = F_12/Inf_size
    f_21_tilde = F_21/Inf_size
    f_22_tilde = F_22/Inf_size
    indices = calculate_indices(f_11, f_12, f_21, f_22, f_1z, f_2z, f_11_tilde, f_12_tilde, f_21_tilde, f_22_tilde)
    return(indices)
    
def generate_sample(f_1z_tilde, f_2z_tilde, f_11_tilde, f_12_tilde, f_21_tilde, f_22_tilde, n_H, n_I):
    sample_dict=dict.fromkeys(['F_1z', 'F_2z', 'F_11', 'F_12', 'F_21', 'F_22'], 0)
    sample_dict.update(zip(['Inf_size', 'Healthy_size'], [n_I, n_H]))
    #print(sample_dict)
    #print(f_1z_tilde)
    #print([f_11_tilde, f_12_tilde, f_21_tilde, f_22_tilde])
    #print(n_H)
    #print(n_I)
    # Generate a random sample from the non-infected population of size n_H
    F_1z_sample=np.random.binomial(n_H,f_1z_tilde)
    sample_dict['F_1z']=F_1z_sample
    F_2z_sample= n_H - F_1z_sample
    sample_dict['F_2z']=F_2z_sample
    # Draw a random sample from the infected part of the population
    Infected_sample = np.random.multinomial(n_I,[f_11_tilde,f_12_tilde,f_21_tilde,f_22_tilde])
    sample_dict.update(zip(['F_11', 'F_12', 'F_21', 'F_22'], Infected_sample))
    #print("created the following sample")
    #print(sample_dict)
    return(sample_dict)
    # Other option to generate dict just at the very end:
    # dict(zip(['F_1z', 'F_2z', 'F_11', 'F_12', 'F_21', 'F_22'], [F_1z_sample, F_2z_sample, *Infected_sample]))

def infcheck(dict):
    Inf_checksum=((int(dict['F_11']>0)) + (int(dict['F_12']>0)) + (int(dict['F_21']>0)) + (int(dict['F_22']>0)))
    return(Inf_checksum)
