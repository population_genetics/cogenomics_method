#!/bin/bash

# Path to the directories with the simulation files
wd="~/Documents/Projects/CSA_project/new_generation_alpha_simul_with_neutral_all/simulation_files_new"
wd_sample2="~/Documents/Projects/CSA_project/generatiopn_simulation_files_005_inf_095_healthy"
wd_sample3="~/Documents/Projects/CSA_project/generation_simulation_files_095_inf_005_healthy"

# Suffixes to appended to simulation set two and three
# The first simulation set will used the default suffix "standard" as encode in the argument
# parser of the R-scripts run
suffix_sample2="005_inf_095_healthy"
suffix_sample3="095_inf_005_healthy"

# Output directory
outdir=~/Documents/Projects/CSA_project/Plots_behavior_Nov2023

if [ ! -d "${outdir}" ] 
then
    mkdir "${outdir}"
fi

# Plot the indices for different alpha/delta values
Rscript --vanilla Plot_indices_stype_alpha_varxaxis.R \
    --wd "${wd}" \
    --phi 0.05 \
    --outdir "${outdir}" \
    --stype sample 

# Plot population vs sample for phi=0.05 and delta=0.1 for nH=1006 and nI=902
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd}" \
    --phi 0.05 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}"

# Plot population vs sample for phi=0.5 and delta=0.1 for nH=1006 and nI=902
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd}" \
    --phi 0.5 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}"

# Plot population vs sample for phi=0.95 and delta=0.1 for nH=1006 and nI=902
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd}" \
    --phi 0.95 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}"

# Plot population vs sample for phi=0.05 and delta=0.2 for nH=1006 and nI=902
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd}" \
    --phi 0.05 \
    --alpha_threshold 0.2 \
    --outdir "${outdir}"

# Plot population vs sample for phi=0.05 and delta=0.3 for nH=1006 and nI=902
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd}" \
    --phi 0.05 \
    --alpha_threshold 0.3 \
    --outdir "${outdir}"

# Plot sample indices for different phi values (0.05, 0.5, 0.95) for delta=0.1 and nH=1006 and nI=902
Rscript --vanilla Plot_indices_stype_phi_varxaxis.R \
    --wd "${wd}" \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --stype sample 

# Plot population indices for different phi values (0.05, 0.5, 0.95) for delta=0.1 and nH=1006 and nI=902
Rscript --vanilla Plot_indices_stype_phi_varxaxis.R \
    --wd "${wd}" \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --stype pop 


# Generate the plots for 95% healthy, 5% infected

# Plot population vs sample for phi=0.05 and delta=0.1 for nH=1813 and nI=95
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample2}" \
    --phi 0.05 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample2}"

# Plot population vs sample for phi=0.05 and delta=0.2 for nH=1813 and nI=95
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample2}" \
    --phi 0.05 \
    --alpha_threshold 0.2 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample2}"

# Plot population vs sample for phi=0.05 and delta=0.3 for nH=1813 and nI=95
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample2}" \
    --phi 0.05 \
    --alpha_threshold 0.3 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample2}"

# Plot sample indices for different phi values (0.05, 0.5, 0.95) for delta=0.1 and nH=1813 and nI=95
Rscript --vanilla Plot_indices_stype_phi_varxaxis.R \
    --wd "${wd_sample2}" \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --stype sample \
    --suffix "${suffix_sample2}"

# Plot population indices for different phi values (0.05, 0.5, 0.95) for delta=0.1 and nH=1813 and nI=95
Rscript --vanilla Plot_indices_stype_phi_varxaxis.R \
    --wd "${wd_sample2}" \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --stype pop \
    --suffix "${suffix_sample2}"

# Plot population vs sample for phi=0.5 and delta=0.1 for nH=1813 and nI=95
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample2}" \
    --phi 0.5 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample2}"

# Plot population vs sample for phi=0.95 and delta=0.1 for nH=1813 and nI=95
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample2}" \
    --phi 0.95 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample2}"

# Generate the plots for 5% healthy, 95% infected sample

# Plot population vs sample for phi=0.05 and delta=0.1 for nH=95 and nI=1813
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample3}" \
    --phi 0.05 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample3}"

# Plot population vs sample for phi=0.05 and delta=0.2 for nH=95 and nI=1813
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample3}" \
    --phi 0.05 \
    --alpha_threshold 0.2 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample3}"

# Plot population vs sample for phi=0.05 and delta=0.3 for nH=95 and nI=1813
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample3}" \
    --phi 0.05 \
    --alpha_threshold 0.3 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample3}"

# Plot sample indices for different phi values (0.05, 0.5, 0.95) for delta=0.1 and nH=95 and nI=1813
Rscript --vanilla Plot_indices_stype_phi_varxaxis.R \
    --wd "${wd_sample3}" \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --stype sample \
    --suffix "${suffix_sample3}"

# Plot population indices for different phi values (0.05, 0.5, 0.95) for delta=0.1 and nH=95 and nI=1813
Rscript --vanilla Plot_indices_stype_phi_varxaxis.R \
    --wd "${wd_sample3}" \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --stype pop \
    --suffix "${suffix_sample3}"

# Plot population vs sample for phi=0.5 and delta=0.1 for nH=95 and nI=1813
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample3}" \
    --phi 0.5 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample3}"

# Plot population vs sample for phi=0.95 and delta=0.1 for nH=95 and nI=1813
Rscript --vanilla Plot_indices_simulations_varxaxis.R \
    --wd "${wd_sample3}" \
    --phi 0.95 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}" \
    --suffix "${suffix_sample3}"