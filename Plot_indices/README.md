# 5.) Plotting of index distributions for various sampling schemes, $\delta$-thresholds and disease prevalences

## Overview

This folder contains all the scripts neccessary to visualize the distribution of the four indices CSA, HS, PI and HP for simulations run with various infection matrices, $\alpha$-thresholds, disease prevalences $\phi$ and sampling schemes of the number of infected and healthy hosts. Indices for the simulations are calculated for both the population and a random sample of infected and healthy hosts.


## Folder contents

- `Plot_indices_simulations.R`: R-script to generate a plot of the index behavior for the population (top row) versus a sample of a given size (bottom row) for a given value of $\phi$ and a given value of $\delta$.

- `Plot_indices_stype_alpha.R`: R-script to generate a plot of the index behavior for $\delta \in \{0.1,0.2,0.3\}$ (rows) for either the sample or the population for a given value of $\phi$.

- `Plot_indices_stype_phi.R`: R-script to generate a plot of the index behavior for $\phi \in \{0.05,0.5,0.95\}$ (rows) for either the sample or the population for a given value of $\delta$.

- `Run_index_analysis_batch.sh`: Script to launch generation of all index distribution plots in the current version of the article.  

- `Plot_indices_simulations_varaxis.R`: R-script to generate a plot of the index behavior for the population (top row) versus a sample of a given size (bottom row) for a given value of $\phi$ and a given value of $\delta$ allowing for **variable x-axis limits**. Script has the command line options as `Plot_indices_simulations.R`. All plots produced using this script end with `*_varxaxis.{pdf|png}`.

- `Plot_indices_stype_alpha_varxaxis.R`: R-script to generate a plot of the index behavior for $\delta \in \{0.1,0.2,0.3\}$ (rows) for either the sample or the population for a given value of $\phi$ allowing for **variable x-axis limits**.  Script has the command line options as `Plot_indices_stype_alpha.R`. All plots produced using this script end with `*_varxaxis.{pdf|png}`.

- `Plot_indices_stype_phi_varxaxis.R`: R-script to generate a plot of the index behavior for $\phi \in \{0.05,0.5,0.95\}$ (rows) for either the sample or the population for a given value of $\delta$ allowing for **variable x-axis limits**.  Script has the command line options as `Plot_indices_stype_phi.R`. All plots produced using this script end with `*_varxaxis.{pdf|png}`.

- `Run_index_analysis_batch_varxaxis.sh`: Script to launch generation of all index distribution plots allowing for variable x-axis limits. Works the same way as  `Run_index_analysis_batch.sh` but calls the corresponding scripts which allow for variable x-axis limits.


## How to Use the Scripts

### Plot_indices_simulations.R

#### Description


#### Libraries

Ensure you have `R` installed along with the following libraries:
- `tidyverse`
- `ggplot2`
- `gridExtra`
- `viridis`
- `argparser`
- `GGally`
- `ggforce`

#### Options

The script can be used via the command line and accepts various arguments:

- `--wd`: Path to the directory with all simulation files. (Default: new_generation_alpha_simul_with_neutral_all/simulation_files_new/)
- `--phi`: $\phi$-value which has been used for the simulations (Default: 0.05)
- `--alpha_threshold`: Value of $\delta$ which has been used for the simualations (Default: 0.1)
- `--outdir`: Name of the output directory (Default: ~/Documents/Projects/Cross_association/)
- `--suffix`: Suffix to appended to all output files (Default: standard)


#### Example Usage:

```{r,eval=F}
Rscript --vanilla Plot_indices_simulations.R \
    --wd "${wd}" \
    --phi 0.05 \
    --alpha_threshold 0.1 \
    --outdir "${outdir}"
```

#### Output
 
- `<outdir>/Index_behavior_phi_<phi>_a_<alpha_threshold>pop_vs_sample_<suffix>.pdf` : Plot of the index behavior for $\phi=$\<phi\> and $\delta$=\<delta_threshold\> for different combinations of the indices for the population and the sample for a given sample size of infected and non-infected individuals in pdf-format.

- `<outdir>/Index_behavior_phi_<phi>_a_<alpha_threshold>pop_vs_sample_<suffix>.pdf` : Plot of the index behavior for $\phi=$\<phi\> and $\delta$=\<delta_threshold\> for different combinations of the indices for the population and the sample for a given sample size of infected and non-infected individuals in png-format.


### Plot_indices_stype_alpha.R

#### Description

This script produces a plot of the index behavior for $\delta \in \{0.1,0.2,0.3\}$ (rows) for either the sample or the population. The us can specify the $\phi$-value of the simulation and if the the plot should be generated for the sample or the population.


#### Libraries

Ensure you have `R` installed along with the following libraries:
- `tidyverse`
- `ggplot2`
- `gridExtra`
- `viridis`
- `argparser`
- `GGally`
- `ggforce`

#### Options

The script can be used via the command line and accepts various arguments:

- `--wd`: Path to the directory with all simulation files. (Default: new_generation_alpha_simul_with_neutral_all/simulation_files_new/)
- `--phi`: $\phi$-value which has been used for the simulations (Default: 0.05)
- `--outdir`: Name of the output directory (Default: ~/Documents/Projects/Cross_association/)
- `--stype`: Should the analysis be run for the population (stype: pop) or samples from the population (stype: sample) (Default: pop)
- `--suffix`: Suffix to appended to all output files (Default: standard)


#### Example Usage:

```{r,eval=F}
Rscript --vanilla Plot_indices_stype_phi.R \
    --wd my_working_dir \
    --phi 0.95 \
    --outdir my_outdir \
    --stype sample 
```

#### Output
 
- `<outdir>/Index_behavior_phi_<phi>_<stype>_for_alpha_<suffix>.pdf` : Plot of the index behavior for $\delta \in \{0.1, 0.2, 0.3\}$ (rows) for different combinations of the indices in pdf-format.

- `<outdir>/Index_behavior_phi_<phi>_<stype>_for_alpha_<suffix>.png` : Plot of the index behavior for $\delta \in \{0.1,0.2,0.3\}$ (rows) for different combinations of the indices in png-format.



### Plot_indices_stype_phi.R

#### Description

This script produces a plot of the index behavior for $\phi \in \{0.05,0.5,0.95\}$ (rows) for either the sample or the population. The used can specify the $\delta$-value of the simulation and if the the plot should be generated for the sample or the population.


#### Libraries

Ensure you have `R` installed along with the following libraries:
- `tidyverse`
- `ggplot2`
- `gridExtra`
- `viridis`
- `argparser`
- `GGally`
- `ggforce`

#### Options

The script can be used via the command line and accepts various arguments:

- `--wd`: Path to the directory with all simulation files. (Default: new_generation_alpha_simul_with_neutral_all/simulation_files_new/)
- `--alpha_threshold`: $\delta$-value which has been used for the simulations (Default: 0.1)
- `--outdir`: Name of the output directory (Default: ~/Documents/Projects/Cross_association/)
- `--stype`: Should the analysis be run for the population (stype: pop) or samples from the population (stype: sample) (Default: pop)
- `--suffix`: Suffix to appended to all output files (Default: standard)


#### Example Usage:

```{r,eval=F}
Rscript --vanilla Plot_indices_stype_phi.R \
    --wd my_working_dir \
    --alpha_threshold 0.1 \
    --outdir my_outdir \
    --stype sample 
```

#### Output
 
- `<outdir>/Index_behavior_a_<alpha_threshold>_<stype>_for_phi_<suffix>.pdf` : Plot of the index behavior for $\phi \in \{0.05,0.5,0.95\}$ (rows) for different combinations of the indices in pdf-format.

- `<outdir>/Index_behavior_a_<alpha_threshold>_<stype>_for_phi_<suffix>.png` : Plot of the index behavior for $\phi \in \{0.05,0.5,0.95\}$ (rows) for different combinations of the indices in png-format.


### Run_index_analysis_batch.sh

#### Description

This bash script can be used to launch generation of all index distribution plots in the current version of the article.

#### Requirements

##### Dependencies


##### Simulation directories

The script searches for the three following directories which contain the simulation results of various analyses presented in our study: **Inference of host-pathogen interaction matrices from genome-wide polymorphism data** ([here](https://www.biorxiv.org/content/10.1101/2023.07.06.547816v1.full))

- `new_generation_alpha_simul_with_neutral_all`: Simulations for a grid of $\delta \in \{0.1,0.2,0.3\}$, $\delta \in \{0.05,0.5,0.95\}$ for $n_I=902$ and $n_H=1006$ with 50,000 simulations for each of the following infection matrices GFG, MA, PI, MA, neutral, iGFG.

- `generation_simulation_files_095_inf_005_healthy`: Simulations for a grid of $\delta \in \{0.1,0.2,0.3\}$, $\delta \in \{0.05,0.5,0.95\}$ for $n_I=1813$ and $n_H=95$ with 50,000 simulations for each of the following infection matrices GFG, MA, PI, MA, neutral, iGFG.

- `generatiopn_simulation_files_005_inf_095_healthy`: Simulations for a grid of $\delta \in \{0.1,0.2,0.3\}$, $\delta \in \{0.05,0.5,0.95\}$ for $n_I=95$ and $n_H=1813$ with 50,000 simulations for each of the following infection matrices GFG, MA, PI, MA, neutral, iGFG.


These folder should contain all the results of the simulations: 
Files should be named as follows:
<Infection_matrix>\_<nb_simulations>\_a<delta>\_phi\_<phi>_run\_<simulation_set>\_<stype>.txt

Where:

- `<Infection_matrix>`:  Name of the infection matrix. Values can be: GFG, MA, PI, MA, neutral, iGFG 

- `<nb_simulations>`: Total number of simulations which have been run for this job. Default: 5000

- `<delta>`: The $\delta$ value used to set up the uniform distributions for random deviations of the matrix coefficients

- `<phi>`  The disease prevalence for the simulation set

- `<simulation_set>` The id of the simulation set. Remember simulations have been split up into several jobs on a SGE and each of these job was used to generate `<nb_simulations>`

Further detail how to run these simulations can be found here: [Further details can be found here](../Generating_alpha_matrix_simulations/README.md)

#### Example Usage:

```{bash,eval=F}
./Run_index_analysis_batch.sh
```

#### Output

Results will be outputted to a directory called: New_plots_behavior_03122023

The following files are created within the directory:

##### Comparisons between population and sample for different values of $\phi$ for different sample sizes

- `Index_behavior_phi_0.05_a_0.1pop_vs_sample_005_inf_095_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.05$ between the population and a sample of $n_I=95$ and $n_H=1813$.

- `Index_behavior_phi_0.5_a_0.1pop_vs_sample_005_inf_095_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.5$ between the population and a sample of $n_I=95$ and $n_H=1813$.

- `Index_behavior_phi_0.95_a_0.1pop_vs_sample_005_inf_095_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.95$ between the population and a sample of $n_I=95$ and $n_H=1813$.

- `Index_behavior_phi_0.05_a_0.1pop_vs_sample_standard.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.05$ between the population and a sample of $n_I=902$ and $n_H=1006$.

- `Index_behavior_phi_0.5_a_0.1pop_vs_sample_standard.{pdf|png}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.5$ between the population and a sample of $n_I=902$ and $n_H=1006$.

- `Index_behavior_phi_0.95_a_0.1pop_vs_sample_standard.{pdf|png}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.95$ between the population and a sample of $n_I=902$ and $n_H=1006$.

- `Index_behavior_phi_0.05_a_0.3pop_vs_sample_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.3$, $\phi=0.05$ between the population and a sample of $n_I=1813$ and $n_H=95$.

- `Index_behavior_phi_0.5_a_0.1pop_vs_sample_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.5$ between the population and a sample of $n_I=1813$ and $n_H=95$.

- `Index_behavior_phi_0.95_a_0.1pop_vs_sample_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.95$ between the population and a sample of $n_I=1813$ and $n_H=95$.

##### Comparison of the behavior of the index for the standard case for different $\delta$-values and for $\phi=0.05$-values for the sample

- `Index_behavior_phi_0.05_sample_for_alpha_standard.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta \in \{0.1,0.2,0.3\}$, $\phi=0.05$ for a sample of $n_I=1813$ and $n_H=1006$.


##### Comparison of the behavior of the index for the standard case for different $\delta$-values and for $\phi=0.05$ for the population

- `Index_behavior_phi_0.05_pop_for_alpha_standard.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.95$ between the population and a sample of $n_I=902$ and $n_H=1006$.

##### Comparison of the behavior of the index for the standard case for different $\alpha$-values and for $\phi=0.05$ for the population

- `Index_behavior_phi_0.05_pop_for_alpha_standard.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta \in \{0.1,0.2,0.3\}$, $\phi=0.05$ for the entire population.

##### Comparison of the behavior of the index for $\delta=0.1$-values and for different $\phi$ values for the population and the sample for different sampling schemes


-  `Index_behavior_a_0.1_sample_for_phi_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi \in \{0.05,0.5,0.95\}$ for a sample of $n_I=1813$ and $n_H=95$.

- `Index_behavior_a_0.1_pop_for_phi_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi \in \{0.05,0.5,0.95\}$  for the population.

- `Index_behavior_a_0.1_sample_for_phi_standard.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi \in \{0.05,0.5,0.95\}$ for a sample of $n_I=1902$ and $n_H=1006$.

- `Index_behavior_a_0.1_pop_for_phi_standard.pdf.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi \in \{0.05,0.5,0.95\}$ for the entire population.

- Index_behavior_a_0.1_sample_for_phi_005_inf_095_healthy.{png|pdf}: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi \in \{0.05,0.5,0.95\}$ for a sample of $n_I=95$ and $n_H=1813$.

- Index_behavior_a_0.1_pop_for_phi_005_inf_095_healthy.{png|pdf}: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi \in \{0.05,0.5,0.95\}$ for the population.

##### Comparisons for the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.2$, $\phi=0.05$ between the population for different sample sizes.

- `Index_behavior_phi_0.05_a_0.2pop_vs_sample_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.2$, $\phi=0.05$ between the population and a sample of $n_I=1813$ and $n_H=95$.

- `Index_behavior_phi_0.05_a_0.2pop_vs_sample_standard.{pdf|png}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.2$, $\phi=0.05$ for the population and a sample of $n_I=1813$ and $n_H=1006$.

- `Index_behavior_phi_0.05_a_0.2pop_vs_sample_005_inf_095_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.2$, $\phi=0.05$ between the population and a sample of $n_I=95$ and $n_H=1813$.

##### Comparisons for the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.3$, $\phi=0.05$ between the population for different sample sizes.

- `Index_behavior_phi_0.05_a_0.3pop_vs_sample_095_inf_005_healthy.{png|pdf}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.1$, $\phi=0.05$ between the population and a sample of $n_I=1813$ and $n_H=95$.

- `Index_behavior_phi_0.05_a_0.3pop_vs_sample_standard.{png|pdf}`: Compares the behavior of the indices 10,000 randomly chosen simulations/model for $\delta=0.3$, $\phi=0.05$ for the population and a sample of $n_I=1813$ and $n_H=1006$.

- `Index_behavior_phi_0.05_a_0.3pop_vs_sample_005_inf_095_healthy.{png|pfd}`: Compares the behavior of the indices for 10,000 randomly chosen simulations/model for $\delta=0.3$, $\phi=0.05$ between the population and a sample of $n_I=95$ and $n_H=1813$.
